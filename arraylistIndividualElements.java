/*
By default, objects are considered equal if they are referred to by the same variable
(the String class is an exception with its pool of String objects). If you want to compare objects
by their state (values of the instance variable), override the equals method in that class.

■ get(int index)—This method returns the element at the specified position in this list.
■ size()—This method returns the number of elements in this list.
■ contains(Object o)—This method returns true if this list contains the speci-
fied element. The method contains defined in the class ArrayList compares two objects by calling
the object’s equals method. It does not compare object references.
■ indexOf(Object o)—This method returns the index of the first occurrence of
the specified element in this list, or –1 if this list doesn’t contain the element.
■ lastIndexOf(Object o)—This method returns the index of the last occurrence of the
specified element in this list, or –1 if this list doesn’t contain the element.
*/
class arraylistIndividualElements{
  public static void main(String... args){
    ArrayList<String> myArrList1 = new ArrayList<String>();
    myArrList1.add("One");
    myArrList1.add("Two");
    String valFromList = myArrList1.get(1);
    System.out.println(valFromList);
    System.out.println(myArrList1.size());

    ArrayList<StringBuilder> myArrList = new ArrayList<StringBuilder>();
    StringBuilder sb1 = new StringBuilder("Jan");
    StringBuilder sb2 = new StringBuilder("Feb");
    myArrList.add(sb1);
    myArrList.add(sb2);
    myArrList.add(sb2);
    System.out.println(myArrList.contains(new StringBuilder("Jan")));
    System.out.println(myArrList.contains(sb1));
    System.out.println(myArrList.indexOf(new StringBuilder("Feb")));
    System.out.println(myArrList.indexOf(sb2));
    System.out.println(myArrList.lastIndexOf(new StringBuilder("Feb")));
    System.out.println(myArrList.lastIndexOf(sb2));
    /*
    false
    true
    -1
    1
    -1
    2
    */
  }
}
