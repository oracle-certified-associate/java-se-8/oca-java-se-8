import java.time.LocalDate;
public class test1{

  public static void main(String... args){

    LocalDate ld1 = LocalDate.parse("2019-02-21");
    System.out.println(ld1.getMonth());
    System.out.println(ld1.getMonthValue());

    char c = '2';
    System.out.print(c);//prints 2

    boolean mybool = Boolean.parseBoolean("abc");
    System.out.print(mybool);//prints false

    boolean mybool2 = Boolean.valueOf("abc");
    System.out.print(mybool2);//prints false

    boolean mybool3 = new Boolean("abc");
    System.out.print(mybool3);//prints false

  }
  void mymethod1(int a){
    System.out.println("mymethod1" + a);
  }
  String mymethod1(int a, int b){
    return " ";
  }
  //below methow won't compile. argument are the same return type is different.
  /*
  String mymethod1(int a){
    return " ";
  }
  */
}
interface BaseInterface1 {
default void getName() { System.out.println("Base 1"); }
} interface BaseInterface2 {
default void getName() { System.out.println("Base 2"); }
} interface MyInterface extends BaseInterface1, BaseInterface2 {
default void getName() { System.out.println("Just me");
BaseInterface1.super.getName();
}
}
class MyClass implements MyInterface{
  public static void main(String[] args){
    MyClass mc = new MyClass();
    mc.getName();
  }
}
