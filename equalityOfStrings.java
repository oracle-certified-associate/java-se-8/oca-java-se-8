/*
The correct way to compare two String values for equality is to use the equals method defined in the String class.
This method returns a true value if the object being compared to it isn’t null, is a String object,
and represents the same sequence of characters as the object to which it’s being compared.

The operator == compares whether the reference variables refer to the same objects, and the method equals
compares the String values for equality. Always use the equals method to compare two Strings for equality.
Never use the == operator for this purpose.
*/
class equalityOfStrings{
  public static void main(String args[]){
  String var1 = new String("Java");
  String var2 = new String("Java");
  System.out.println(var1.equals(var2));//prints true
  System.out.println(var1 == var2);//prints false
  System.out.println(var1 != var2);//prints true
  /*
  The operator == compares the reference variables, that is, whether the variables refer to the same object.
  */

  String var3 = "code";
  String var4 = "code";
  System.out.println(var3.equals(var4));//prints true
  System.out.println(var3 == var4);//prints true
  /*
  The variables var3 and var4 refer to the same String object created and shared in the pool of String objects.
  */

  String lang1 = "Java";
  String lang2 = "JaScala";
  String returnValue1 = lang1.substring(0,1);
  String returnValue2 = lang2.substring(0,1);
  System.out.println(returnValue1);
  System.out.println(returnValue2);
  System.out.println(returnValue1 == returnValue2);//false
  System.out.println(returnValue1.equals(returnValue2));//true
  /*
  Watch out for the exam questions that test you on using the == operator with String values
  returned by methods of the class String. Because these values are created using the new operator,
  they aren’t placed in the String pool.
  */
  }
}
