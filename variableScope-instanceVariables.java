/*
Instance is another name for an object. Hence, an instance variable is available for the
life of an object. An instance variable is declared within a class, outside all the methods.
It’s accessible to all the instance (or nonstatic) methods defined in a class.

The scope of an instance variable is longer than that of a local variable
or a method parameter.
*/
class Phone {
  private boolean tested;
  public void setTested(boolean val) {
    tested = val;//accessible
  }
  public boolean isTested() {
    return tested;//accessible
  }
}
