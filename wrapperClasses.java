/*
Java defines a wrapper class for each of its primitive data types.
Boolean, Character, Byte, Short, Integer, Long, Float, Double
The wrapper classes are used to wrap primitives in an object, so they can be added to a collection object.
They enable all types to be treated like object instances. Wrapper classes help you
write cleaner code, which is easy to read. All the wrapper classes are immutable—classes that don’t allow changes
to the state of their instances after initialization.
All the numeric wrapper classes extend the class java.lang.Number. Classes Boolean and Character directly extend the class Object.
All the wrapper classes implement the interfaces "java.io.Serializable" and "java.lang.Comparable". All these classes
can be serialized to a stream, and their objects define a natural sort order

You can create objects of all the wrapper classes in multiple ways:
■ Assignment—By assigning a primitive to a wrapper class variable (autoboxing)
■ Constructor—By using wrapper class constructors
■ Static methods—By calling static method of wrapper classes, like, valueOf()
*/
public class wrapperClasses{
  public static void main(String... args){
    //all of the below are correct:

    //autoboxing
    Boolean bool1 = true;
    Character char1 = 'a';
    Byte byte1 = 10;
    Double double1 = 10.98;
    //Constructors that accept primitive value
    Boolean bool2 = new Boolean(true);
    Character char2 = new Character('a');
    Byte byte2 = new Byte((byte)10);
    Double double2 = new Double(10.98);
    //Constructors that accept string
    /*
    All wrapper classes (except Character) define a constructor that accepts a String argument representing
    the primitive value that needs to be wrapped. Watch out for exam questions that include a call
    to a no-argument constructor of a wrapper class. None of these classes define a no-argument constructor.
    */
    Boolean bool3 = new Boolean("true");
    Byte byte3 = new Byte("10");
    Double double3 = new Double("10.98");
    //Using static method valueOf()
    Boolean bool4 = Boolean.valueOf(true);
    Boolean bool5 = Boolean.valueOf(true);
    Boolean bool6 = Boolean.valueOf("TrUE");
    Double double4 = Double.valueOf(10);

    //Each wrapper class (except Character) defines a method to parse a String to the corresponding primitive value.
    Long.parseLong("12434");
    Byte.parseByte("123");
    Boolean.parseBoolean("true");
    Boolean.parseBoolean("TrUe");
    boolean mybool = Boolean.parseBoolean("hello");
    /*
    Casting to string only works when the object actually is a string
    It won't work when the object is something else
    System.out.println((string)mybool);//won't work
    */
    System.out.println(String.valueOf(mybool));
    /*
    All parse methods (listed in table 2.14) throw NumberFormat- Exception except Boolean.parseBoolean().
    This method returns false whenever the string it parses is not equal to “true”
    */

    /*
    DETAILS:
    Wrapper classes Byte, Short, Integer, and Long cache objects with values in the range of -128 to 127.
    The Character class caches objects with values 0 to 127. These classes define inner static classes
    that store objects for the primitive values -128 to 127 or 0 to 127 in an array.
    If you request an object of any of these classes, from this range, the valueOf() method returns a reference
    to a predefined object; otherwise, it creates a new object and returns its reference.

    Wrapper classes Float and Double don’t cache objects for any range of values.
    */

    Long var1 = Long.valueOf(123);
    Long var2 = Long.valueOf("123");
    System.out.println(var1 == var2); //prints true

    Long var3 = Long.valueOf(223);
    Long var4 = Long.valueOf(223);
    System.out.println(var3 == var4); //prints false. var3 and var4 refer to different objects

    /*
    DETAILS:
    Objects of different wrapper classes with same values are not equal.
    Using equals() with such instances will return false.
    If you use == with such instances, the code won’t compile.
    */
    Integer obj1 = 100;
    Short obj2 = 100;
    System.out.println(obj1.equals(obj2)); //outputs false
    //System.out.println(obj1 == obj2); //doesn't compile
  }
}
