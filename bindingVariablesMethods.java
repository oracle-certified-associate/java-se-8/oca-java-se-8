/*
You can use reference variables of a base class to refer to an object of a derived class.
But there’s a major difference in how Java accesses the variables and methods for
these objects. With inheritance, the instance variables bind at compile time and the
methods bind at runtime.

Binding refers to resolving of variables or methods that would be called
for a reference variable.
*/
class bindingVariablesMethods{
  public static void main(String args[]){
    Employee emp = new Employee();
    Employee programmer = new Programmer();
    System.out.println(emp.name);

    /*
    DETAIL
    Variables are bound at compile time. Because type of variable programmer is Employee,
    below line accesses the variable name defined in class Employee.

    */
    System.out.println(programmer.name);
    emp.printName();
    programmer.printName();
    programmer.myMethod();
    /*
    Output is:
    Employee
    Employee
    Employee
    Programmer
    programmerMethod
    */
  }
}
class Employee {
  String name = "Employee";
  void printName() {
    System.out.println(name);
  }
  void myMethod(){
    System.out.println("employeeMethod");
  }
}
class Programmer extends Employee {
  String name = "Programmer";
  void printName() {
    System.out.println(name);
  }
  void myMethod(){
    System.out.println("programmerMethod");
  }
}
/*
DETAIL
Method "myMethod" is defined in both Employee class and Programmer class. On line 25, programmer is a variable of type Employee and refers to an object of type Programmer.
When this line is executed, "myMethod" in Programmer class is executed.
If we delete "myMethod" in Employee class then this line(25), although it invokes the method in Programmer class, would fail to compile!
*/
