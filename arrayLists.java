/*
Package java.util isn’t implicitly imported into your class so you have to import it explicitly.
An ArrayList uses an array to store its elements. It provides you with the functionality of a dynamic array.
It offers you all the benefits of using an array with none of the disadvantages.
It looks and behaves like an expandable array that’s modifiable.
*/
import java.util.ArrayList;
import java.util.ListIterator;
class arrayLists{
  public static void main(String... args){
    ArrayList<String> myArrList1 = new ArrayList<String>();
    ArrayList<String> myArrList2 = new ArrayList<>();//staring with Java 7 you can omit object type on the right hand side.

    /*
    When you add an element to the end of the list, the ArrayList first checks whether its instance variable
    elementData has an empty slot at the end.
    If there’s an empty slot at its end, it stores the element at the first available empty slot.
    If no empty slots exist, the method ensureCapacity creates another array with a higher capacity and
    copies the existing values to this newly created array. It then copies the newly added value at the first
    available empty slot in the array.
    When you add an element at a particular position, an ArrayList creates a new array
    (only if there’s not enough room left) and inserts all its elements at positions other than the position you specified.
    If there are any subsequent elements to the right of the position that you specified,
    it shifts them by one position. Then it adds the new element at the requested position.
    */
    ArrayList<String> myArrList3 = new ArrayList<>();
    myArrList3.add("one");
    myArrList3.add("two");
    myArrList3.add("four");
    myArrList3.add(2, "three");

    myArrList3.set(2, "3"); // replace an element in an ArrayLilst


    ListIterator<String> iterator = myArrList3.listIterator();
    while (iterator.hasNext()) {
      System.out.println(iterator.next());
    }
    //below for loop and above ListIterator output the same values.
    for (String element : myArrList3) {
      System.out.println(element);
    }

    /*
    You can also modify the existing values of an ArrayList by accessing its individual
    elements. Because Strings are immutable, let’s try this with StringBuilder. Here’s
    the code:
    */
    myArrList3.add(new StringBuilder("O-ne"));
    myArrList3.add(new StringBuilder("T-wo"));
    myArrList3.add(new StringBuilder("T-hree"));
    for (StringBuilder element : myArrList3)
      element.append(element.length());//append the length of each value at the end to modify the objects in the Arraylist.
    for (StringBuilder element : myArrList3)
      System.out.println(element);


  }
}
