/*

*/
class arrayDeclarationAllocationInitialization{
  public static void main(String args[]){
    /*
    When you combine an array declaration, allocation, and initialization
    in a single step, you can’t specify the size of the array. The size of the
    array is calculated by the number of values that are assigned to the array.
    */
    int intArray1[] = {0, 1};//without using new keyword
    int intArray2[] = new int[]{0, 1};//using new keyword

    int multiArray1[][] = { {0, 1}, {3, 4, 5} };//without using new keyword
    int multiArray2[][] = new int[][]{ {0, 1}, {3, 4, 5}};//using new keyword

    /*
    If you try to specify the size of an array with the preceding approach, the code
    won’t compile!!
    int intArray2[] = new int[2]{0, 1}; -> won't compile
    */

    /*
    if you declare and initialize an array using two separate lines of code,
    you need to use the keyword new to initialize the values.
    */
    int intArray3[];
    intArray3 = new int[]{0, 1};
    /*
    int intArray[];
    intArray = {0, 1}; -> won't compile
    */
  }
}
