/*
The terms encapsulation and information hiding are used interchangeably.
By exposing object functionality only through methods, you can
prevent your private variables from being assigned any values that don’t fit
your requirements. One of the best ways to create a well-encapsulated class is
to define its instance variables as private variables and allow access to these
variables using public methods.
*/
class Phone {
  private double weight;
  public void setWeight(double val) {
    if (val >= 0 && val <= 1000) {
      weight =val;
    }
  }
  public double getWeight() {
    return weight;
  }
}
class Home {
  public static void main(String[] args) {
    Phone ph = new Phone();
    ph.setWeight(-12.23);
    System.out.println(ph.getWeight());//prints 0
    ph.setWeight(77712.23);
    System.out.println(ph.getWeight());//prints 0
    ph.setWeight(12.23);
    System.out.println(ph.getWeight());//prints 12.23
  }
}
