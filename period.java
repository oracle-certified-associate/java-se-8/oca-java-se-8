/*
You can add or subtract Period instances from the LocalDate and LocalDateTime classes.
Period is also an immutable class and hence safe to use in a multithreaded environment.
With a private constructor, the Period class defines multiple factory methods to create its instances.

A period of 35 days is not stored as 1 month and 5 days. Its individual elements,
that is, days, months, and years, are stored the way it is initialized.

You can define positive or negative periods of time. For example,
you can define Period instances representing 15 or -15 days.
*/
import java.time.Period;
import java.time.LocalDate;
import java.time.LocalDateTime;
class period{
  public static void main(String args[]){
    Period period1 = Period.of(1, 2, 7);
    Period period2 = Period.ofYears(2);
    Period period3 = Period.ofMonths(5);
    Period period4 = Period.ofWeeks(10);
    Period period5 = Period.ofDays(15);

/*
You can also parse a string to instantiate Period by using its static method parse.
This method parses string values of the format PnYnMnD or PnW, where n represents a
number and the letters (P, Y, M, D, and W) represent parse, year, month, day, and week.

If you pre- cede the complete string value passed to parse() with a negative sign (-),
it’s applied to all values. If you place a negative sign just before an individual number,
it applies only to that section.

If you pass invalid string values to parse(), the code will compile but will throw a runtime exception.
*/

    //all of the below represent p5y
    Period p5Yrs1 = Period.parse("P5y");
    Period p5Yrs2 = Period.parse("p5y");
    Period p5Yrs3 = Period.parse("P5Y");
    Period p5Yrs4 = Period.parse("+P5Y");
    Period p5Yrs5 = Period.parse("P+5Y");
    Period p5Yrs6 = Period.parse("-P-5Y");//check this out
    System.out.println(p5Yrs1 + ":" + p5Yrs2);

    Period p5Yrs7 = Period.parse("P5y1m2d");
    Period p5Yrs8 = Period.parse("p9m");
    Period p5Yrs9 = Period.parse("P60d");
    Period p5Yrs10 = Period.parse("-P5W");
    System.out.println(p5Yrs7);
    System.out.println(p5Yrs8);
    System.out.println(p5Yrs9);
    System.out.println(p5Yrs10);//DETAIL: For the string format PnW, the count of weeks is multiplied by 7 to get the number of days.!

    //You can also use the static method between(LocalDate dateInclusive, LocalDate dateExclusive!!) to instantiate Period
    //period = end date – start date
    LocalDate carnivalStart = LocalDate.of(2050, 12, 31);
    LocalDate carnivalEnd = LocalDate.of(2051, 1, 2);
    Period periodBetween = Period.between(carnivalStart, carnivalEnd);
    System.out.println("period between: " + periodBetween);

    //manipulating LocalDate and LocalDateTime (plusand minus methods with Period)
    LocalDate date = LocalDate.of(2052, 01, 31);
    System.out.println(date.plus(Period.ofDays(1)));//2052-02-01

    LocalDateTime dateTime = LocalDateTime.parse("2052-01-31T14:18:36");
    System.out.println(dateTime.plus(Period.ofMonths(1)));//2052-02-29T14:18:36 DETAIL! not 30 days

    LocalDateTime dateTime2 = LocalDateTime.parse("2020-01-31T14:18:36");
    System.out.println(dateTime2.minus(Period.ofYears(2)));

    LocalDate date2 = LocalDate.of(2052, 01, 31);
    System.out.println(date2.minus(Period.ofWeeks(4)));//2052-01-03 DETAIL! 4*7=28

    /*
    You can use the instance methods getYears(), getMonths(), and getDays()
    to query a Period instance on its years, months, and days. All these methods return an int value.
    When you initialize a Period instance with days more than 31 or months more than 12,
    it doesn’t recalculate its years, months, or days components.
    */
    Period period = Period.of(2,4,40);
    System.out.println(period.getYears());
    System.out.println(period.getMonths());
    System.out.println(period.getDays());

    /*
    DETAIL
    You can query whether any of three units of a Period is negative using the methods isNegative and isZero.
    A Period instance is zero if all three units are zero. The isNegative method returns true if at least
    one of its three components is strictly negative (<0):
    */
    Period days5 = Period.of(0,0,5);
    System.out.println(days5.isZero());//false
    Period daysMinus5 = Period.of(0,1,-5);
    System.out.println(daysMinus5.isNegative());//true

    /*
    DETAIL
    In the class Period, both the getXXX() methods and minusXXX() methods
    use the plural form: getYears(), minusHours(). Different than LocalDate, LocalTime and LocalDateTime!!

    DETAIL
    What happens when you subtract a Period representing one month (P1M) from a Period representing 10 days (P10D)?
    Adding a Period of 10 months to a Period of 5 months gives 15 months, not 1 year and 3 months.
    */
    Period period10Days = Period.of(0, 0, 10);
    Period period1Month = Period.of(0, 1, 0);
    System.out.println(period10Days.minus(period1Month));//P-1M10D
    System.out.println(period10Days.minusDays(5));//P5D
    System.out.println(period10Days.minusMonths(5));//P-5M10D
    System.out.println(period10Days.minusYears(5));//P-5Y10D

    Period period5Month = Period.of(0, 5, 0);
    Period period10Month = Period.of(0, 10, 0);
    System.out.println(period5Month.plus(period10Month));
    System.out.println(period10Days.plusDays(35));
    System.out.println(period10Days.plusMonths(5));
    System.out.println(period10Days.plusYears(5));

    /*
    The Period class defines multipliedBy(int), which multiplies each element in the period by the integer value.
    */
    Period year1Month9Day20 = Period.of(1, 9, 20);
    System.out.println(year1Month9Day20.multipliedBy(2));
    System.out.println(year1Month9Day20.multipliedBy(-2));

    //The withDays(), withMonths(), and withYears() methods accept an int value and
    //return a copy of Period with the specified value altered.
    Period period56Month = Period.of(0, 6, 0);
    System.out.println(period56Month.withDays(2));

    System.out.println(Period.of(10,5,40).toTotalMonths());//125 DETAIL

    LocalDate dtTest = LocalDate.of(2020, 5, 23);
    System.out.println(dtTest.plus(Period.of(10,5,40)));//2030-12-02 DETAIL Years are handled first, then montsh and days.
  }
}
