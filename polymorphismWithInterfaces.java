/*
Whereas polymorphism with classes has a class as the base class,
polymorphism with interfaces requires a class to implement an interface.

Polymorphism with interfaces involves abstract or default
methods from the implemented interface.

DETAIL
An interface can also define static methods, but static methods never participate in polymorphism!
*/
class polymorphismWithInterfaces{
  public static void main(String args[]){
    MobileAppExpert expert1 = new Programmer();
    MobileAppExpert expert2 = new Manager();
    expert1.deliverMobileApp();
    expert2.deliverMobileApp();

/*
Below wouldn't compile.  Method is not visible to Employee class.
    Employee expert1 = new Programmer();
    Employee expert2 = new Manager();
    expert1.deliverMobileApp();
    expert2.deliverMobileApp();
*/

    Interviewer2 interviewer2 = new Manager2();
    interviewer2.submitInterviewStatus();
    Manager2 mgr2 = new Manager2();
    mgr2.submitInterviewStatus();
  }
}

//polymorphism with abstract methods
interface MobileAppExpert {
  void deliverMobileApp();
}
class Employee {}
class Programmer extends Employee implements MobileAppExpert {
  public void deliverMobileApp() {
    System.out.println("testing complete on real device");
  }
}
class Manager extends Employee implements MobileAppExpert {
  public void deliverMobileApp() {
    System.out.println("QA complete");
    System.out.println("code delivered with release notes");
  }
}

//polymorphism with default methods
interface Interviewer2 {
  default Object submitInterviewStatus() {
    System.out.println("Interviewer:Accept");
    return null;
  }
}
class Manager2 implements Interviewer2 {
  public String submitInterviewStatus() {
    System.out.println("Manager:Accept");
    return null;
  }
}

/*
DETAIL
The below code compiles successfully and outputs Just me. Using the interface
names BaseInterface1, BaseInterface2 in the declaration of class MyClass is redundant
(duplicate) because MyInterface already extends BaseInterface1 and BaseInterface2.
So MyClass doesn’t inherit three implementations of the default method
getName. It inherits just one of them, getName(), which is defined in the interface
MyInterface.
*/
interface BaseInterface1 {
  default void getName() { System.out.println("Base 1"); }
}
interface BaseInterface2 {
  default void getName() { System.out.println("Base 2"); }
}
interface MyInterface extends BaseInterface1, BaseInterface2 {
  default void getName() { System.out.println("Just me"); }
}
class MyClass implements BaseInterface1, BaseInterface2, MyInterface {
  public static void main(String ar[]) {
    new MyClass().getName();
  }
}
