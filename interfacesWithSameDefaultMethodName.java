/*
Imagine a class, Animal, that extends multiple interfaces, Moveable and Jumpable,
which define default methods with the same name, relax(). If the class Animal doesn’t
override the default implementation of relax(), it won’t compile.
*/
interface Jumpable {
  default void relax() {
    System.out.println("No jumping");
  }
}
interface Moveable {
  default void relax() {
    System.out.println("No moving");
  }
}
//class Animal implements Jumpable, Moveable { } //-> This won't compile
class Animal implements Jumpable, Moveable {
  public void relax() {
    System.out.println("Watch movie");
  }
}
/*
The default methods that a class inherits from the interfaces that it implements must
form a correct set of overloaded methods, or else the class won’t compile.
*/
interface Jumpable2 {
  default void relax() {
    System.out.println("No jumping");
  }
}
interface Moveable2 {
  default String relax() {
    System.out.println("No moving");
    return null;
  }
}
//class Animal implements Jumpable2, Moveable2 { } //-> This won't compile

//observe the example below carefully
interface BaseInterface1 {
  default void getName() {
    System.out.println("Base 1");
  }
}
interface BaseInterface2 {
  default void getName() {
    System.out.println("Base 2");
  }
}
interface MyInterface extends BaseInterface1, BaseInterface2 {
  default void getName() {
    System.out.println("Just me..");
    BaseInterface1.super.getName();
  }
}
class MyClass implements MyInterface{
  public static void main(String[] args){
    MyClass mc = new MyClass();
    mc.getName();
  }
}
