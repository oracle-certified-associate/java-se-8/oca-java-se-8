/*
There are 2 cases:
■ When a method reassigns the object reference passed to it to another variable
■ WHEN METHODS MODIFY THE STATE OF THE OBJECT REFERENCES PASSED TO THEM <---

When a method is passed a reference value, a copy of the reference (that
is, the memory address) is passed to the invoked method. The callee can do whatever it
wants with its copy without ever altering the original reference held by the caller.

In the preceding code, B creates two object references, person1 and person2.
line 37 prints John:Paul—the value of person1.name and person2.name.
The code then calls the method swap and passes to it the objects referred to by
person1 and person2. When these objects are passed as arguments to the method
swap, the method arguments p1 and p2 also refer to these objects.
*/
class Person {
  private String name;
  Person(String newName) {
    name = newName;
  }
  public String getName() {
    return name;
  }
  public void setName(String val) {
    name = val;
  }
}
class Test {
  public static void resetValueOfMemberVariable(Person p1) {
    p1.setName("Rodrigue");
  }
  public static void main(String args[]) {
    Person person1 = new Person("John");
    System.out.println(person1.getName());//prints John
    resetValueOfMemberVariable(person1);
    System.out.println(person1.getName());//print Rodrigue
  }
}
/*
The method resetValueOfMemberVariable accepts the object referred to by person1
and assigns it to the method parameter p1. Now both variables, person1 and p1, refer
to the same object. p1.setName("Rodrigue") modifies the value of the object referred
to by the variable p1. Because the variable person1 also refers to the same object,
person1.getName() returns the new name, Rodrigue, in the method main.
*/
