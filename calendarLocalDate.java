/*
LocalDate can be used to store dates like 2015-12-27 without time or time zones.
LocalDate instances are immutable and hence safe to be used in a multithreaded environment.
LocalDate is immutable. All the methods (minusXX, plusXX, withXX etc) that seem to manipulate its value return a copy
of the LocalDate instance on which it’s called.
*/
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
class calendarLocalDate{
  public static void main(String args[]){

    LocalDate date1 = LocalDate.of(2015, 12, 27);
    LocalDate date2 = LocalDate.of(2015, Month.DECEMBER, 27);
    LocalDate date3 = LocalDate.now();
    LocalDate date4 = LocalDate.parse("2025-08-09");

    LocalDate date = LocalDate.parse("2020-08-30");
    System.out.println(date.getDayOfMonth());
    System.out.println(date.getDayOfWeek());
    System.out.println(date.getDayOfYear());
    System.out.println(date.getMonth());
    System.out.println(date.getMonthValue());
    System.out.println(date.getYear());

    LocalDate suleBday = LocalDate.parse("1986-08-20");
    LocalDate serdarBday = LocalDate.parse("1980-03-18");
    System.out.println(suleBday.isAfter(serdarBday));//true
    System.out.println(suleBday.isBefore(serdarBday));//false

    /*
    LocalDate is immutable. All the methods (minusXX, plusXX, withXX etc) that seem to manipulate its value return a copy
    of the LocalDate instance on which it’s called.
    */
    LocalDate bday = LocalDate.of(2052,03,10);
    System.out.println(bday.minusDays(10));
    System.out.println(bday.minusMonths(2));
    System.out.println(bday.minusWeeks(30));
    System.out.println(bday.minusYears(1));

    LocalDate launchCompany = LocalDate.of(2016,02,29);
    System.out.println(launchCompany.plusDays(1));
    System.out.println(launchCompany.plusMonths(1));
    System.out.println(launchCompany.plusWeeks(7));
    System.out.println(launchCompany.plusYears(1));

    /*
    withXX methods return a copy of the date instance replacing the specified day, month or year in it..
    */
    LocalDate someTime = LocalDate.of(2036,02,28);
    System.out.println(someTime.withDayOfMonth(1));
    System.out.println(someTime.withDayOfYear(1));//2036-01-01
    System.out.println(someTime.withMonth(7));
    System.out.println(someTime.withYear(1));//0001-02-28

    /*
    The LocalDate class defines overloaded atTime() instance methods. These meth- ods combine LocalDate with
    time to create and return LocalDateTime.
    If you pass any invalid hours, minutes, or seconds value to the method atTime, it will throw a DateTimeException at runtime.
    */
    LocalDate interviewDate = LocalDate.of(2016,02,28);
    System.out.println(interviewDate.atTime(16, 30));
    System.out.println(interviewDate.atTime(16, 30, 20));
    System.out.println(interviewDate.atTime(16, 30, 20, 300));
    System.out.println(interviewDate.atTime(LocalTime.of(16, 30)));

    /*
    You can use the method toEpochDay() to convert LocalDate to the epoch date — the
    count of days from January 1, 1970
    */
    LocalDate launchBook = LocalDate.of(2016,2,8);
    System.out.println(launchBook.toEpochDay());//16839
  }
}
