/*
The operators == (equal to) and != (not equal to) can be used to compare all types of
primitives: char, byte, short, int, long, float, double, and boolean.

It’s a very common mistake to use the assignment operator, =, in place of the equality
operator, ==, to compare primitive values.
*/
public class relationalOperators{
  public static void main(String... args){
    int a = 10;
    int b = 20;
    System.out.println(a=b); //prints 20. This is not comparison, so result is not a boolean value!
  }
}
