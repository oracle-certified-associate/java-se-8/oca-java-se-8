/*
A static method in an interface can’t be called using a reference
variable. It must be called using the interface name.
In contrast, you can call a static method defined in a class
either by using reference variables or by the name of the class.
*/
interface Interviewer {
  abstract void conductInterview();
  default void submitInterviewStatus() {
      System.out.println("Accept");
  }
  static void bookConferenceRoom(LocalDateTime dateTime, int duration) {
    System.out.println("Interview scheduled on:" + dateTime);
    System.out.println("Book conference room for: "+duration + " hrs");
  }
}

class Manager implements Interviewer {}
class Project {
  public static void main(String[] args) {
    Interviewer inv = new Manager();
    inv.bookConferenceRoom(LocalDateTime.now(), 2);//won't compile
    Manager mgr = new Manager();
    mgr.bookConferenceRoom(LocalDateTime.now(), 2);//won't compile
    Interviewer.bookConferenceRoom(LocalDateTime.now(), 2);//compiles
  }
}
