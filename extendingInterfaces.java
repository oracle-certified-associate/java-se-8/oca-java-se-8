/*
Below, the getName method in MyInterface can refer to a superinterface
method by using the super keyword:
■ BaseInterface1.super.getName();
■ BaseInterface2.super.getName(); (This would work as well if MyInterface
were a class implementing both interfaces.) Other methods too can invoke a
superinterface method this way.
*/

interface BaseInterface1 {
  default void getName() { System.out.println("Base 1"); }
}
interface BaseInterface2 {
  default void getName() { System.out.println("Base 2"); }
}
//interface MyInterface extends BaseInterface1, BaseInterface2 {}  -> won't compile
interface MyInterface extends BaseInterface1, BaseInterface2 {
  default void getName() {
    BaseInterface1.super.getName();
    BaseInterface2.super.getName();
    System.out.println("MyInterface");
    //super.getName(); // this line won't compile.
  }
}

class MyClass implements MyInterface{
    public static void main(String... args){
      MyClass mc = new MyClass();
      mc.getName();
    }
}
