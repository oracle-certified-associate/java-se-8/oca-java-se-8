/*
■ Overloaded methods must have method parameters different from one another.
■ Overloaded methods may or may not define a different return type.
■ Overloaded methods may or may not define different access levels.
■ Overloaded methods can’t be defined by only changing their return type or
access modifiers or both.
*/
class MyClass {
  double calcAverage(double marks1, int marks2) {
    return (marks1 + marks2)/2.0;
  }
  double calcAverage(int marks1, double marks2) {
    return (marks1 + marks2)/2.0;
  }
  public static void main(String args[]) {
    MyClass myClass = new MyClass();
    myClass.calcAverage(2, 3);//fails to compile. Compiler can't determine which overloaded method should be called.
  }
}
