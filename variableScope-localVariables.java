/*
Local variables are defined within a method. They may or may not be defined within
code constructs such as if-else constructs, looping constructs, or switch statements.
Typically, you’d use local variables to store the intermediate results of a calculation.
Compared to the other three variable scopes listed previously, they have the shortest
scope (life span).


*/
class Student {
  private double marks1, marks2, marks3;
  private double maxMarks = 100;
  public double getAverage() {
    double avg = 0;
    avg = ((marks1 + marks2 + marks3) / (maxMarks*3)) * 100;
    if(avg == 0){
      int myvar = 0;
    }
    else{
      myvar = 1; //won't compile. myvar is not accssible. it is local to if block.
    }
    return avg;
  }
  public void setAverage(double val) {
    avg = val; //won't compile. avg is not accessible outside getAverage method.
  }
}
