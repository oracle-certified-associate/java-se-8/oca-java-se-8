/*
The class StringBuilder is defined in the package java.lang, and it has a mutable sequence of characters.
You should use the class StringBuilder when you’re deal- ing with larger strings or modifying the contents
of a string often. Doing so will improve the performance of your code. Unlike StringBuilder,
the String class has an immutable sequence of characters. Every time you modify a string that’s represented
by the String class, your code creates new String objects instead of modifying the existing one.

Because StringBuilder represents a mutable sequence of characters, the main operations on StringBuilder
are related to the modification of its value by adding another value at the end or at a particular position,
deleting characters, or changing characters at a particular position.

Index Usage:
- insert/append
- delete/replace/subSequence
*/
class mutableStringBuilder{
  public static void main(String args[]){
    //You can create objects of the class StringBuilder using multiple overloaded constructors, as follows:
    StringBuilder sb1 = new StringBuilder();//no-argument constructor. Constructs a StringBuilder object with no characters in it and an initial capacity of 16 characters.
    StringBuilder sb2 = new StringBuilder(sb1);//constructor that accepts a StringBuilder object
    StringBuilder sb3 = new StringBuilder(50);//Constructor that accepts an int value specifying initial capacity of StringBuilder object
    StringBuilder sb4 = new StringBuilder("Shreya Gupta");//Creates an array of length (16 + str.length)
    /*
    The append method adds the specified value at the end of the existing value of a StringBuilder object.
    Because you may want to add data from multiple data types to a StringBuilder object,
    this method has been overloaded so that it can accept data of any type.
    This method accepts all the primitives, String, char array, and Object, as method parameters
    */
    sb1.append("Java");
    sb1.append(new Person("Serdar"));
    System.out.println(sb1);//prints JavaPerson@7852e922 (the hex value (7852e922) that follows the @ sign may differ on your system)
    sb1.append(true);
    System.out.println(sb1);//JavaPerson@7852e922true
    char name2[] = {'a','b','c','d','e'};
    sb1.append(name2, 1, 2);//starting at position 1 append 2 characters (char at 1 included)
    System.out.println(sb1);//JavaPerson@7852e922truebc
    /*
    the default implementation of the method toString in the class Object returns the name of the class followed
    by the @ char and unsigned hexadecimal representation of the hash code of the object (the value returned by
    the object’s hashCode method).
    If the toString method has been overridden by the class, then the method append adds the String value
    returned by it to the target StringBuilder object.
    */

    /*
    The main difference between the append and insert methods is that the insert method enables you to
    insert the requested data at a particular position, but the append method allows you to
    add the requested data only at the end of the StringBuilder object.
    */
    sb1.insert(4, "Inserted");//JavaInsertedPerson@7852e922truebc
    System.out.println(sb1);

    StringBuilder sb5 = new StringBuilder("123");
    char[] name = {'J', 'a', 'v', 'a'};
    sb5.insert(1, name, 1, 3);//insert at position 1 of sb5, 3 characters from "name" starting at postion 1 (starting at postion 1 insert 3 characters)
    System.out.println(sb5);//1ava23

    //DETAIL: See the difference between usage of indices for append/insert methods and delete method
    StringBuilder sb6 = new StringBuilder("0123456");
    sb6.delete(2, 4);//Removes characters at positions starting from 2 to 4, excluding 4. The method delete(2,4) doesn’t delete the character at position 4.
    System.out.println(sb6);//01456
    sb6.deleteCharAt(2);
    System.out.println(sb6);//0156

    //As the name suggests, the reverse method reverses the sequence of characters of a StringBuilder.
    //You can’t use the method reverse to reverse a substring of StringBuilder.
    sb6.reverse();
    System.out.println(sb6);//6510
    /*
    TRIM: Unlike the class String, the class StringBuilder doesn’t define the method trim.
    */
    /*
    Unlike the replace method defined in the class String, the replace method in the class StringBuilder
    replaces a sequence of characters, identified by their positions, with another String.
    DETAIL: Observe the usage of indices. It is similar to delete method.
    */
    sb6.replace(2, 4, "ABCD");
    System.out.println(sb6);//65ABCD

    /*
    Usage of the indices is similar to delete and replace methods.
    */
    System.out.println(sb6.subSequence(2, 4));//AB
    System.out.println(sb6);//65ABCD -> The method subsequence doesn’t modify the existing value of a StringBuilder object.
  }
}
class Person {
    String name;
    Person(String str) { name = str; }
}
