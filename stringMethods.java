/*
Let's divide the methods of String class into 3 categories:
Query position of chars:
- charAt
- indexOf
- substring

Seem to modify a "String":
- substring
- trim
- replace

Others:
- length
- startsWith
- endsWith
*/
class stringMethods{
  public static void main(String args[]){
    String name = new String("Paul");
    System.out.println(name.charAt(0));//prints P
    System.out.println(name.charAt(2));//prints u
    //System.out.println(name.charAt(4)); --> this would throw an exception at runtime

    String letters = "ABCAB";
    System.out.println(letters.indexOf('B'));//prints 1
    System.out.println(letters.indexOf("B"));//prints 1
    System.out.println(letters.indexOf("S"));//prints -1
    System.out.println(letters.indexOf("CA"));//prints 2
    System.out.println(letters.indexOf('B', 2));//starts searching at index 2. prints 4

    String exam = "Oracle";
    String sub = exam.substring(2);
    System.out.println(sub);//prints acle
    String result = exam.substring(2, 4);
    System.out.println(result);//prints ac -> char at position4 is not included!!

    String varWithSpaces = " AB CB    ";
    System.out.println(varWithSpaces);
    System.out.println(varWithSpaces.trim());
    //The trim() method returns a new String by removing
    //all the leading and trailing white space in a String (new lines, spaces, or tabs)
    //Note that this method doesn’t remove the space within a String.

    System.out.println(letters.replace('B', 'b'));//prints AbCAb
    System.out.println(letters.replace("CA", "12"));//prints AB12B
    System.out.println(letters);//prints ABCAB

    System.out.println("Shreya".length());//prints 6

    /*
    DETAIL:
    The methods startsWith and endsWith accept only arguments of type String.
    The method charAt accepts only method arguments of type int.
    Hence, this method can be passed char values, which are stored as unsigned integer values.
    */
    System.out.println(letters.startsWith("AB"));//true
    System.out.println(letters.startsWith("a"));//false
    System.out.println(letters.startsWith("A", 3));//true

    System.out.println(letters.endsWith("CAB"));//true
    System.out.println(letters.endsWith("B"));//true
    System.out.println(letters.endsWith("b"));//false

    //METHOD CHAINING
    String result2 = "Sunday ".replace(' ', 'Z').trim().concat("M n");//When chained, the methods are evaluated from left to right.
    System.out.println(result2);//prints SundayZZM n
    /*
    Because String objects are immutable, their values won’t change if you execute
    methods on them. You can, of course, reassign a value to a reference variable of type String. See below.
    */
    String day = "SunDday";
    day.replace('D', 'Z').substring(3);
    System.out.println(day);//prints SunDday
    day = day.replace('D', 'Z').substring(3);
    System.out.println(day);//prints Zday

    /*
    Behind the scenes, string concatenation is implemented by using the StringBuilder
    (covered in the next section) or StringBuffer (similar to StringBuilder) classes.
    But remember that a String is immutable. You can’t modify the value of any exist- ing object of String.
    The + operator enables you to create a new object of the class String with a value equal to the
    concatenated values of multiple Strings.
    */

    int num = 10;
    int val = 12;
    String aStr = "OCJA";
    String anotherStr = num + val + aStr;
    System.out.println(anotherStr);//prints 22OCJA
    /*
    If you wish to treat the numbers stored in variables num and val as String values,
    modify the expression as follows:
    */
    String anotherStr2 = "" + num + val + aStr;
    System.out.println(anotherStr2);//prints 1012OCJA

    String lang = "Java";
    lang += " is everywhere!";
    String initializedToNull = null;
    initializedToNull += "Java";
    System.out.println(initializedToNull);//prints nullJava
    /*
    When you use += to concatenate String values, ensure that the variable
    you’re using has been initialized (and doesn’t contain null).
    */
  }
}
