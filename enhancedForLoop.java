/*
The enhanced for loop is also called the for-each loop, and it offers some advantages
over the regular for loop. It also has some limitations.
*/
import java.util.ArrayList;
import java.util.Iterator;
class enhancedForLoop{
  public static void main(String[] args){
    ArrayList<String> myList= new ArrayList<String>();
    myList.add("Java");
    myList.add("Loop");
    //below are 2 loops. First one is a regular for loop, second is an enhanced for loop.
    //as you can see it is much easier to iterate through an ArrayList using enhanced for loop
    for(Iterator<String> i = myList.iterator(); i.hasNext();)
      System.out.println(i.next());
    for (String val : myList)//You can read the colon (:) in a for-each loop as "in"
      System.out.println(val);

    /*
    Nested ArrayList and nested Enhenced for Loop
    */
    ArrayList<String> exams= new ArrayList<String>();
    exams.add("Java");
    exams.add("Oracle");
    ArrayList<String> levels= new ArrayList<String>();
    levels.add("Basic");
    levels.add("Advanced");
    ArrayList<String> grades= new ArrayList<String>();
    grades.add("Pass");
    grades.add("Fail");

    ArrayList<ArrayList<String>> nestedArrayList = new ArrayList< ArrayList<String>>();
    nestedArrayList.add(exams);
    nestedArrayList.add(levels);
    nestedArrayList.add(grades);

    for (ArrayList<String> nestedListElement : nestedArrayList)
      for (String element : nestedListElement)
        System.out.println(element);

    /*
    DETAIL : pass by value / pass by reference
    What happens when you try to modify the value of the loop variable in an enhanced
    for loop? The result depends on whether you’re iterating through a collection of
    primitive values or objects. If you’re iterating through an array of primitive values,
    manipulation of the loop variable will never change the value of the array being iterated
    because the primitive values are passed by value to the loop variable in an enhanced
    for loop.
    When you iterate through a collection of objects, the value of the collection is
    passed by reference to the loop variable. Therefore, if the value of the loop variable
    is manipulated by executing methods on it, the modified value will be reflected in the
    collection of objects being iterated
    */
    StringBuilder myArr[] = {
      new StringBuilder("Java"),
      new StringBuilder("Loop")
    };
    for (StringBuilder val : myArr)//iterates through myArr
      System.out.println(val);
    for (StringBuilder val : myArr)//Appends "Oracle" to the value referenced by the loop variable "val"
      val.append("Oracle");
    for (StringBuilder val : myArr)//assigns new StringBuilder object to reference variable "val" with value Oracle
      val = new StringBuilder("Oracle");
    for (StringBuilder val : myArr)//prints JavaOracle and LoopOracle
      System.out.println(val);

//exams, levels, grades are defined above
    for (String exam : exams)
      for (String level : levels)
        for (String grade : grades)
          System.out.println(exam+":"+level+":"+grade);
  }
}
