/*
When a unary operator is used in an expression, its placement with respect to its operand
decides whether its value will increment or decrement before the evaluation of
the expression or after the evaluation of the expression
*/
public class unaryOperators{

  public static void main(String... args){
    int a = 20;
    int b = 10;
    int c = a - ++b;
    System.out.println(c); //prints 9
    System.out.println(b); //prints 11

    int a = 50;
    int b = 10;
    int c = a - b++;
    System.out.println(c); //prints 40
    System.out.println(b); //prints 11

    int a = 10;
    a = a++ + a + a-- - a-- + ++a;
    System.out.println(a); //prints 32, a = 10 + 11 + 11 - 10 + 10;
  }

}
