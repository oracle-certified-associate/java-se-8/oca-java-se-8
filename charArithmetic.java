/*
You can use all arithmetic operators with the char primitive data
type, including unary increment and decrement operators.

When you apply the addition operator to char values, their corresponding ASCII values
are added and subtracted.

This one will output:
194
0
194
*/
public class charArithmetic{
  public static void main(String args[]){
    char myChar = 'a';
    System.out.println(myChar*2);
    System.out.println(myChar - myChar);
    System.out.println(myChar + myChar);
  }
}
