/*
Logical operators are used to evaluate one or more expressions. These expressions
should return a boolean value. You can use the logical operators AND, OR, and NOT to
check multiple conditions and proceed accordingly.

If you wish to proceed with a task when both the
conditions are true, use the logical AND operator, &&. If you wish to proceed with a task
when either of the conditions is true, use the logical OR operator, ||. If you wish to
reverse the outcome of a boolean value, use the negation operator, !.

Operator && (AND)
true && true -> true
true && false -> false
false && true -> false
false && false -> false
true && true && false ->false

Operator || (OR)
true || true -> true
true || false -> true
false || true -> true
false || false -> false
false || false || true ->true

Operator ! (NOT)
!true -> false
!false -> true
*/
public class logicalOperators(){
  public static void main(String... args){
    int marks = 8;
    int total = 10;
    /*
    && and || are also called short circuit operators
    If the first operand to && operator evaluates to false, the result can never be true. Therefore, &&
    does not evaluate the second operand.
    Similarly, the || operator does not evaluate the second operator if the first operand evaluates to true.
    */
    System.out.println(total < marks && ++marks > 5);
    System.out.println(marks);//prints 8 since the second operand is not evaluated
    System.out.println(total == 10 || ++marks > 10);
    System.out.println(marks);//prints 8 since the second operand is not evaluated

    int a = 10;
    int b = 20;
    int c = 40;
    System.out.println(a >= 99 && a <= 33 || b == 10);
    //QUESTION: Which operands are evaluated in this expression??
    /*
    EXPLANATION:
    Expression three operands together with the OR (||) and AND (&&) short-circuit operators.
    Because the short-circuit operator AND has higher operator precedence than the short-circuit operator OR,
    the expression is evaluated as follows:
    ((a >= 99 && a <= 33) || b == 10 )

    a >= 99 evaluates to false, so the next operand (a <= 33) isn’t evaluated. Because
    the first operand to operator ||, a >= 99 && a <= 33), evaluates to false, b == 10 is
    evaluated.
    */
  }
}
