/*
At line 17, when php is set to null, the instance referred to by it isn't eligible for garbage collection
because it can still be referenced by "java.other".
At line 18, when java is also set to null, both of the objects referred to by php and java become eligible
for garbage collection.
Even though both of these objects can be referred to by each other, they can no longer be referenced in the main method.
They form an "island of isolation".
*/
class Exam {
  private String name;
  private Exam other;
  public Exam(String name) {
    this.name = name;
  }
  public void setExam(Exam exam) {
    other = exam;
  }
}
class IslandOfIsolation {
  public static void main(String args[]) {
    Exam php = new Exam("PHP");
    Exam java = new Exam("Java");
    php.setExam(java);
    java.setExam(php);
    php = null;
    java = null;//line 18
  }
}
