/*
For arithmetic operations with data types char, byte, short, or
int, all operand values are widened to int. If an arithmetic operation
includes the data type long, all operand values are widened to long. If an
arithmetic operation includes a data type of float or double, all operand values
are widened to double.
*/
public class implicitWidening{
  public static void main(String[] args){
    byte var1 = 10;
    byte var2 = 20;
    short sum = var1 + var2;
    //fails to compile since we are trying to but int into short.
    //Because all byte, short, and char values are automatically
    //widened to int when used as operands for arithmetic operations.
    //This compiles if var1 and var2 are set as "final" since compiler
    //will then know that no dataloss will happen.
  }

}
