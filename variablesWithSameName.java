
class MyPhone {
  static boolean softKeyboard = true;
  boolean softKeyboard = true;//Won’t compile. Class variable and instance variable can’t be defined using the same name in a class.
  String phoneNumber;

  void myMethod(int weight) {
    int weight = 10;  //Won’t compile. Method parameter and local variable can’t be defined using the same name in a method.
  }

  void myMethod2() {
    boolean softKeyboard = true;//Local variable softKeyboard can coexist with class variable softKeyboard
    String phoneNumber;//Local variable phoneNumber can coexist with instance variable phoneNumber
  }
}
