interface MyInterface {}
class MyClass1 implements MyInterface {}
class MyClass2 implements MyInterface {}
class arraysOfInterfaceAbstractClassObject {
  public static void main(String... args){
    /*
    If the type of an array is an interface, its elements are either null or objects that
    implement the relevant interface type. For example, for the interface MyInterface,
    the array interfaceArray can store references to objects of either the class MyClass1
    or MyClass2.
    */
    MyInterface[] interfaceArray = new MyInterface[]
    {
      new MyClass1(),
      null,
      new MyClass2()
    };
    /*
    If the type of an array is an abstract class, its elements are either null or objects of
    concrete classes that extend the relevant abstract class.
    */
    abstract class Vehicle{}
    class Car extends Vehicle {}
    class Bus extends Vehicle {}
    Vehicle[] vehicleArray = {
      new Car(),
      new Bus(),
      null
    };

    /*
    Because all classes extend the class java.lang.Object, elements of an array whose
    type is java.lang.Object can refer to any object.
    */
    Object[] objArray = new Object[] {
      new MyClass1(),
      null,//null is a valid element
      new Car(),
      new java.util.Date(),
      new String("name"),
      new Integer [7] //Array element of type Object can refer to another array
    };
  }
}
