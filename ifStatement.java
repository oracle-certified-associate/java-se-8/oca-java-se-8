/*
Notice that the assignment operation allow = true assigns the value true to
the boolean variable allow. Also, its result is also a boolean value, which
makes it eligible to be passed on as an argument to the if construct.
*/
class ifStatement{
  public static void main(String... args){
    boolean allow = true;
    if(allow = false){//this is not an comparison but an assignment
      System.out.println("true");//prints "true" DETAIL
    }
    else{
      System.out.println("false");//if it was if(allow = false) this line would print "false"
    }
  }
}
