/*
With Java 8, you can add methods to an interface without breaking the existing
implementations.
Prior to Java 8, an interface could only define abstract methods.
With Java 8, an interface can define the default implementation for
its methods (so it doesn’t stop the existing classes that implement it from compiling).
Interfaces in Java 8 can also define static methods.
Although Java doesn’t allow a class to inherit from more than one class, it allows a
class to implement multiple interfaces.
The declaration of an interface can’t include a class name. An interface can never extend any class.

DETAIL
All methods of an interface are implicitly public.
The interface variables are implicitly public, static, and final.

Why do you think these implicit modifiers are added to the interface members?
Because an interface is used to define a contract, it doesn’t make sense to limit access
to its members—and so they are implicitly public. An interface can’t be instantiated,
and so the value of its variables should be defined and accessible in a static context,
which makes them implicitly static.

DETAIL
All the top-level Java types (classes, enums, and interfaces) can be declared using
only two access levels: public and default. Inner or nested types can be declared using any access level.

Valid access modifiers for an interface:
If you try to declare your top-level interfaces by using the other access modifiers
(protected or private), your interface will fail to compile.

Valid access modifiers for members of an interface:
All members of an interface—variables, methods, inner interfaces, and inner classes
(yes, an interface can define a class within it!) are inherently public because that’s
the only modifier they can accept. Using other access modifiers results in compilation errors.
*/
import java.time.LocalDateTime;
class Employee {
  String name;
  String address;
  String phoneNumber;
  float experience;
}
interface Trainable {
  public void attendTraining();
}
interface Interviewer {
  public void conductInterview();
  static void bookConferenceRoom(LocalDateTime dateTime, int dur) {
        System.out.println("Interviewer-bookConferenceRoom");
  }
}
/*
DETAIL
If an interface defines a static method, the class that implements it can define a
static method with the same name, but the method in the interface isn’t related to the
method defined in the class.
*/
class Manager extends Employee implements Interviewer, Trainable {
  int teamSize;
  void reportProjectStatus() {}
  public void conductInterview() {
    System.out.println("Mgr - conductInterview");
  }
  public void attendTraining() {
    System.out.println("Mgr - attendTraining");
  }
  static String bookConferenceRoom(LocalDateTime dateTime, int dur) {
    System.out.println("Manager-bookConferenceRoom");
    return null;
  }
}
class Programmer extends Employee implements Trainable{
  String[] programmingLanguages;
  void writeCode() {}
  public void attendTraining() {
    System.out.println("Prog - attendTraining");
  }
}
/*
The methods of an interface are implicitly public. When you implement an interface,
you must implement all its methods by using the access modifier public. A class that
implements an interface can’t make the interface’s methods more restrictive.

DETAIL
Although the following class and interface definitions look acceptable, they’re not:
*/
interface Relocatable {
void move();
}
class CEO implements Relocatable {
void move() {}
}
