/*
LocalTime stores time in the format hours-minutes-seconds (without a time
zone) and to nanosecond precision.
Like LocalDate, LocalTime is also immutable and hence safe to be used in a multithreaded environment.
The LocalTime constructor is private, so you must use one of the factory methods to
instantiate it. The static method of() accepts hours, minutes, seconds, and nanoseconds.
LocalTime doesn’t define a method to pass a.m. or p.m. Use values 0–23 to define hours.

DETAIL:
What happens if you pass out-of-range values for hours, minutes, or seconds to of()? In this case, you’ll get
a runtime exception, DateTimeException. You’ll get a compiler error if the range of
values passed to a method doesn’t comply with the method’s argument type.
LocalTime timeHrsMin = LocalTime.of(120, 12); -> runtime exception
LocalTime timeHrsMin2 = LocalTime.of(9986545781, 12); -> compiler error

LocalTime Constants:
LocalTime.MIN—Minimum supported time, that is, 00:00
LocalTime.MAX—Maximum supported time, that is, 23:59:59.999999999
LocalTime.MIDNIGHT—Time when the day starts, that is, 00:00
LocalTime.NOON—Noontime, that is, 12:00
*/
import java.time.LocalTime;
import java.time.LocalDate;
import java.time.LocalDateTime;
class calendarLocalTime{
  public static void main(String... args){
    LocalTime lt1 = LocalTime.of(12, 12);
    LocalTime lt2 = LocalTime.of(0, 12, 6);
    LocalTime lt3 = LocalTime.of(14, 7, 10, 998654578);
    System.out.println(lt1);//12:12
    System.out.println(lt2);//00:12:06
    System.out.println(lt3);//14:07:10.998654578

    LocalTime lt4 = LocalTime.now();
    System.out.println(lt4);
    /*
    //If you pass invalid string values to parse(), the code will compile but will throw a runtime exception
    The hours and minutes values passed to parse() must be two digits;
    a single digit is considered an invalid value. For hours and minutes with the value 0–9, pass 00–09.
    */
    LocalTime time = LocalTime.parse("15:08:23");
    System.out.println(LocalTime.MIN.equals(LocalTime.MIDNIGHT));//true

    /*
    Querying LocalTime
    You can use instance methods like getXX() to query LocalTime on its hour, minutes,
    seconds, and nanoseconds.
    DETAIL:
    Watch out for exam questions that use invalid method names like getHours(), getMinutes(), getSeconds(), or getNanoSeconds()!!
    */
    LocalTime myTime = LocalTime.of(16, 20, 12, 98547);
    System.out.println(myTime.getHour());
    System.out.println(myTime.getMinute());
    System.out.println(myTime.getSecond());
    System.out.println(myTime.getNano());

    /*
    You can use the instance methods isAfter() and isBefore() to check whether a
    time is after or before the specified time.
    */
    LocalTime suleFinishTime = LocalTime.parse("17:09:04");
    LocalTime serdarFinishTime = LocalTime.parse("17:09:12");
    if(suleFinishTime.isBefore(serdarFinishTime))
      System.out.println("Sule wins");
    else
      System.out.println("Serdar wins");

    /*
    Manipulating LocalTime:
    You can use the instance methods minusHours(), minusMinutes(), minusSeconds(),
    and minusNanos() to create and return a copy of LocalTime instances with the specified
    period subtracted.
    DETAIL:
    Unlike the getXXX() methods, minusXXX() methods use the plural form: getHour() versus minusHours(), getMinute() versus minusMinutes(),
    getSecond() versus minusSeconds(), and getNano() versus minusNanos().
    */
    LocalTime movieStartTime = LocalTime.parse("21:00:00");
    int commuteMins = 35;
    LocalTime suleStartTime = movieStartTime.minusMinutes(commuteMins);
    System.out.print("Movie starts at: " + movieStartTime + ". ");
    System.out.println("Start cycling by " + suleStartTime + " from office");

    int worldRecord = 10;
    LocalTime raceStartTime = LocalTime.of(8, 10, 55);
    LocalTime raceEndTime = LocalTime.of(8, 11, 11);
    if (raceStartTime.plusSeconds(worldRecord).isAfter(raceEndTime))
      System.out.println("New world record");
    else
      System.out.println("Try harder");
      /*
      The withHour(), withMinute(), withSecond(), and withNano() methods accept an
      int value and return a copy of LocalTime with the specified value altered.
      */
      LocalTime startTime = LocalTime.of(5, 7, 9);
      if (startTime.getMinute() < 30)
        startTime = startTime.withMinute(0);
      System.out.println(startTime);//05:00:09
      /*
      The LocalTime class defines the atDate() method to combine a LocalDate with itself
      to create LocalDateTime. Remember, LocalDate has an atTime() method.
      */
      LocalTime ltime = LocalTime.of(14, 10, 0);
      LocalDate date = LocalDate.of(2016,02,28);
      LocalDateTime dateTime = ltime.atDate(date);
      LocalDateTime dateTime2 = date.atTime(ltime);
      System.out.println(dateTime);//2016-02-28T14:10
      System.out.println(dateTime2);//2016-02-28T14:10
  }
}
