/*
this and super are implicit object references. These variables are defined and initialized
by the JVM for every object in its memory. The this reference always points to an object’s own instance.

Below, in the context of constructor methods (or any methods for that matter), the local variable will take precedence.
Using name, address within the scope of the Employee class constructor block will implicitly refer to that method’s
parameter, not the instance variable. In order to refer to the instance variable name
from within the scope of the Employee class constructor, you are "obliged" to use a this reference.

The keywords super and this are implicit object references. Because static methods
belong to a class, not to objects of a class, you can’t use this and super in static
methods.
*/
class usingThis{
  public static void main(String args[]){
    Interviewer m = new Manager();
    m.submitInterviewStatus();
  }
}
class Employee {
  String name;
  String address;
  Employee() {
    name = "NoName";
    address = "NoAddress";
  }
  Employee(String name) {
    this.name = name;
  }
  Employee(String name, String address) {
    //this(); // "this()" can be used to call the default no-argument constructor
    this(name);//Referencing one contructor from another. Calls constructor that accepts only name.
    this.address = address;//assigns method parameter to instance variable
  }
}
/*
DETAIL
With Java 8, you can use the keyword this in an interface’s "default method" to access
its constants and other default and abstract methods.
*/
interface Interviewer {
  int MIN_SAL = 9999;
  default void submitInterviewStatus() {
    System.out.println(this);
    System.out.println(this.MIN_SAL);
    System.out.println(this.print());
  }
  String print();
}
class Manager implements Interviewer {
  public String print() {
    return("I am " + this);
  }
}
