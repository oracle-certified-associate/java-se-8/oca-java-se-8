/*
There are differences, when you try to access an object using a reference
variable of "its own type", "its base class", or an "implemented interface". Let’s start with
accessing an object with a variable of its own type.
*/
class Employee {
  String name;
  String address;
  String phoneNumber;
  float experience;
}

interface Interviewer {
  public void conductInterview();
}
class HRExecutive extends Employee implements Interviewer {
  String[] specialization;
  public void conductInterview() {
    System.out.println("HRExecutive - conducting interview");
  }
}
class Office {
  public static void main(String args[]) {
    // 1 - Using a variable of the derived class to access its own object
    HRExecutive hr = new HRExecutive();
    /*
    When you access an object of the class HRExecutive using its own type, you can access
    all the variables and methods that are defined in its base class and interface the class
    Employee and the interface Interviewer.
    */
    hr.specialization = new String[] {"Staffing"};
    System.out.println(hr.specialization[0]);
    hr.name = "Sule Serdar";
    System.out.println(hr.name);
    hr.conductInterview();

    // 2 - Using a variable of a superclass to access an object of a derived class
    Employee emp = new HRExecutive();
    /*
    The variable emp can see only the Employee object. Hence,
    it can access only the variables and methods defined in the class Employee
    */
    //emp.specialization = new String[] {"Staffing"}; -> won't compile
    //System.out.println(emp.specialization[0]); -> won't compile
    emp.name = "Pavni Gupta";
    System.out.println(emp.name);
    //emp.conductInterview(); -> won't compile

    // 3 - Using a variable of an implemented interface to access a derived class object
    Interviewer interviewer = new HRExecutive();
    /*
    Variable interviewer can’t access members of class Employee or HRExecutive
    Picture it like this: the variable interviewer can only access the methods defined in the interface Interviewer.
    */
    //interviewer.specialization = new String[] {"Staffing"}; -> won't compile
    //System.out.println(interviewer.specialization[0]); -> won't compile
    //interviewer.name = "Pavni Gupta"; -> won't compile
    //System.out.println(interviewer.name); -> won't compile
    interviewer.conductInterview(); // Can access the method defined in interface Interviewer

    /*
    You can assign an object of HRExecutive to any of the following types of variables:
    ■ HRExecutive
    ■ Employee
    ■ Interviewer
    ■ Object
    */
  }
}
