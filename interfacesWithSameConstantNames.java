/*
A class can implement multiple interfaces with the same constant name, as long as a
call to these interfaces isn’t ambiguous.
*/
interface Jumpable {
  int MIN_DISTANCE = 10;
}
interface Moveable {
  String MIN_DISTANCE = "SMALL";
}
class Animal1 implements Jumpable, Moveable {}
/* This won't compile. Call to MIN_DISTANCE is ambiguous
class Animal2 implements Jumpable, Moveable {
  Animal2() {
    System.out.println(MIN_DISTANCE);
  }
}
*/
class Animal3 implements Jumpable, Moveable {
  Animal3() {
    System.out.println(Jumpable.MIN_DISTANCE);//compiles.  Reference to MIN_DISTANCE is not ambiguous
  }
}

interface Jumpable2 {
  int MIN_DISTANCE = 10;
}
interface Moveable2 {
  String MAX_DISTANCE = "SMALL";
}
/*
A class can implement multiple interfaces with the same constant
names, only if a reference to the constants isn’t ambiguous.
*/
class Animal implements Jumpable2, Moveable2 {
  Animal() {
    System.out.println(MIN_DISTANCE);
  }
}
