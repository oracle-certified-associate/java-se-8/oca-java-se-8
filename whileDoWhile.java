/*
Both of these
loops work in the same manner except for one difference: the while loops checks its
condition before evaluating its loop body, and the do-while loop checks its condition
after executing the statements defined in its loop body.
*/
class whileDoWhile{
  public static void main(String... args){
    int num = 9;
    boolean divisibleBy7 = false;
    while (!divisibleBy7) {
      System.out.println(num);
      if (num % 7 == 0) divisibleBy7 = true;
      --num;
    }
    int num2 = 9;
    boolean divisibleBy_7 = false;
    do {
      System.out.println(num2);
      if (num2 % 7 == 0)
        divisibleBy_7 = true;
      num2--;
    } while (divisibleBy_7 == false);

    int num3=10;
    do {
      num3++;
    } while (++num3 > 20);
    System.out.println (num3);//prints 12 ! DETAIL

    int num4=10;
    while (++num4 > 20) {
      num4++;
    } System.out.println(num4);//prints 11 ! DETAIL
  }
}
