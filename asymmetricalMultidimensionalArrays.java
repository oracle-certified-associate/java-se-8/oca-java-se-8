/*
A multidimensional array can be asymmetrical.
*/
class asymmetricalMultidimensionalArrays{
  public static void main(String... args){
    /*
    In the example below, multiStrArr[1] refers to a null value. An attempt to
    access any element of this array, such as multiStrArr[1][0], will throw an exception.
    */
    String multiStrArr[][] = new String[][]{{"A", "B"}, null, {"Jan", "Feb", "Mar"}};

    //String multiStrArr[][] = new String[][]{{"A", "B"}, null, {"Jan", "Feb", "Mar"}, {1,2,3}};
    //for the above line compiler will throw 3 errors for each of the integers {1,2,3}  ->  incompatible types: int cannot be converted to String
  }
}
