/*
  A switch statement accepts arguments of types char, byte, short, int, and String
  (starting in Java version 7). It also accepts arguments and expressions of types enum,
  Character, Byte, Integer, and Short. The switch statement
  doesn’t accept arguments of type long, float, or double, or any object besides
  String.

  Watch out for questions in the exam that try to pass a primitive
  decimal type such as float or double to a switch statement. Code that tries
  to do so will not compile.

  DETAIL
  See the code below. You can assign "int" to "long" but can't assign "Integer" to "Long" (wrapper classes)
*/
class ternary{
  public static void main(String[] args){
      long bill = 2000;
      //int discount = (bill > 2000)? bill-100 : bill - 50; // won't compile because of lossy conversion

      int bill2 = 2000;
      long discount2 = (bill2 > 2000)? bill2-100 : bill2 - 50;//compiles

      //Long discount3 = (5000 > 2000)? new Integer(10) : new Integer(15);//won't compile. incompatible types

      //Nested ternary constructsswitch
      int bill = 2000;
      int qty = 10;
      int days = 10;
      int discount = (bill > 1000)? (qty > 11)? 10 : days > 9? 20 : 30 : 5;
      System.out.println(discount);//prints 20
  }
}
