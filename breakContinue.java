/*
The break statement is used to exit—or break out of—the for, for-each, do, and
do-while loops, as well as switch constructs.
Alternatively, the continue statement can be used to skip the remaining steps
in the current iteration and start with the next loop iteration.

When you use the break statement with nested loops, it exits the inner loop.

When you use the continue statement with nested loops, it exits the current iteration
of the inner loop.

In Java, you can add labels to the following types of statements:
■ A code block defined using {}
■ All looping statements (for, enhanced for, while, do-while)
■ Conditional constructs (if and switch statements)
■ Expressions
■ Assignments
■ return statements
■ try blocks
■ throws statements

You can use a labeled break statement to exit an outer loop.
You can use a labeled continue statement to skip an iteration of the outer loop.
*/
class breakContinue{
  public static void main(String... args){
    String[] programmers = {"Sule", "Shreya", "Selvan", "Harry"};
    //As soon as a loop encounters a break, it exits the loop. Hence, only the first value of this array—that is, Paul—is printed.
    for (String name : programmers) {
      if (name.equals("Shreya"))
        break;
      System.out.println(name);
    }

    //below loops print Outer:Outer: -> break exits inner loop
    String[] programmers2 = {"Outer", "Inner"};
    for (String outer : programmers2) {
      for (String inner : programmers2) {
        if (inner.equals("Inner"))
          break;
        System.out.println(inner + ":");
      }
    }
    //in the below loop "Shreya" is not printed
    for (String name : programmers) {
      if (name.equals("Shreya"))
        continue;
      System.out.println(name);
    }

    //You can use a labeled break statement to exit an outer loop.
    String[] programmers3 = {"Outer", "Inner"};
    outer:
    for (String outer : programmers3) {
      for (String inner : programmers3) {
        if (inner.equals("Inner"))
          break outer;
        System.out.println(inner + ":");//prints "Outer:"
      }
    }

    String[] programmers4 = {"Sule", "Shreya", "Selvan", "Harry"};
    outer:
    for (String name1 : programmers4) {
      for (String name : programmers4) {
        if (name.equals("Shreya"))
          continue outer;
        System.out.println(name);// prints "Paul" 4 times
      }
    }
  }
}
