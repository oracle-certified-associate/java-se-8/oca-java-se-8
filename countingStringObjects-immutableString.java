/*
To test your understanding of the various ways in which a String object can be created,
the exam may question you on the total number of String objects created in a given piece of code.
Count the total number of String objects created in the following code.
*/
class ContString {
    public static void main(String... args) {
    String summer = new String("Summer");// 1- String object created
    String summer2 = "Summer";// 2- creates a new String object with the value "Summer" and places it in the String constant pool.
    System.out.println("Summer");//no new String object created. onject in the String constant pool is used.
    System.out.println("autumn");// 3- new String object created and placed in the pool
    System.out.println("autumn" == "summer");//4- String object for "autumn" is re-sed from the pool. A new String object is created for "summer" (java is case sensitive)
    String autumn = new String("Summer");//5- A new String object is created whenever "new" keyword is used.
  }
}
/*
String OBJECTS ARE IMMUTABLE:

JVM creates a pool of String objects that can be referenced by multiple variables across the JVM.
The JVM can make this opti- mization only because String is immutable.
Once created, the contents of an object of the class String can never be modified. The immutability of
String objects helps the JVM reuse String objects, reducing memory overhead and increasing performance.

String objects can be shared across multiple reference variables without any fear of changes in their values.
If the refer- ence variables str1 and str2 refer to the same String object value "Java",
str1 need not worry for its lifetime that the value "Java" might be changed through the variable str2.

How Does the implementation of String class make it immutable??
1) STRING USES A CHAR ARRAY TO STORE ITS VALUE - The arrays are fixed in size—they can’t grow once they’re initialized.
2) STRING USES FINAL VARIABLE TO STORE ITS VALUE - A final variable can be initialized with a value only once.
3) METHODS OF STRING DON’T MODIFY THE CHAR ARRAY - All the methods defined in the class String,
such as substring, concat, toLower- Case, toUpperCase, trim, and so on, which seem to modify
the contents of the String object on which they’re called,
create and return a new String object rather than modify the existing value.

*/
