/*
What happens if you modify the common object references in these lists, myArrList
and yourArrList? We have two cases here: in the first one, you reassign the object reference
using either of the lists. In this case, the value in the second list will remain
unchanged. In the second case, you modify the internals of any of the common list elements—
in this case, the change will be reflected in both of the lists.

- addAll
- clear
*/
import java.util.ArrayList;
class arraylistOtherMethods{
  public static void main(String... args){
    //Adding multiple elements to an Arraylist
    ArrayList<String> myArrList = new ArrayList<String>();
    myArrList.add("One");
    myArrList.add("Two");
    ArrayList<String> yourArrList = new ArrayList<>();
    yourArrList.add("Three");
    String fr = "Four";
    yourArrList.add(fr);
    myArrList.addAll(1, yourArrList);//addAll
    for (String val : myArrList)
      System.out.println(val);//one three four two

    myArrList.set(1, "3");
    for (String val : myArrList)
      System.out.println(val);//one 3 four two
    for (String val : yourArrList)
      System.out.println(val);//three four

    /*
    Clearing ArrayList elements
    */
    yourArrList.clear();
    for (String val : yourArrList)
      System.out.println(val);//prints nothing.

    yourArrList.add("cleared");
    for (String val : yourArrList)
      System.out.println(val);//prints cleared.
  }
}
