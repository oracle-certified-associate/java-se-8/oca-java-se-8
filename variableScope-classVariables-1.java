/*
A class variable is defined by using the keyword static. A class variable belongs to a
class, not to individual objects of the class. A class variable is shared across all objects—
objects don’t have a separate copy of the class variables. You don’t even need an object to access a class variable.
It can be accessed by using the name of the class in which it’s defined.
*/
/*
REMEMBER: In a single source file you can define only 1 "public" class.
And if you have a public class, name of that class should be the same as the name of the source file.
We don't have that situation in this example. I just wanted to mention that.

The scope of the class variable softKeyboard depends on its access
modifier and that of the Phone class. Because the class Phone and the class variable
softKeyboard are defined using default access, they’re accessible only within the
package com.mobile.

If you encounter Java Error: Could not find or load main class refer to -> http://big.info/2018/01/java-error-not-find-load-main-class.html
*/
package com.mobile;

class TestPhone {
  public static void main(String[] args) {
    Phone.softKeyboard = false;
    Phone p1 = new Phone();
    Phone p2 = new Phone();
    System.out.println(p1.softKeyboard);
    System.out.println(p2.softKeyboard);
    p1.softKeyboard = true;
    System.out.println(p1.softKeyboard);
    System.out.println(p2.softKeyboard);
    System.out.println(Phone.softKeyboard);
    /*
    When you access static
    variable softKeyboard, Java refers to the type of reference variables p1 and p2 (which
    is Phone) and not to the objects referred to by them. So accessing a static variable
    using a null reference won’t throw an exception:
    */
    Phone p1 = null;
    System.out.println(p1.softKeyboard);
  }
}
class Phone {
  static boolean softKeyboard = true;
}
