/*

*/
class casting{
  public static void main(String... args){
    Interviewer interviewer = new HRExecutive();
    //interviewer.specialization = new String[] {"Staffing"}; //-> won't compile
    /*
    The compiler knows that the type of the variable
    interviewer is Interviewer and that the interface Interviewer doesn’t define
    any variable with the name specialization.
    */

    /*
    On the other hand, the JRE knows that the object referred to by the variable
    interviewer is of type HRExecutive, so you can use casting to get past the Java compiler
    and access the members of the object being referred to, as follows:
    */
    ((HRExecutive)interviewer).specialization = new String[] {"Staffing"};
    /*
    In the previous example code, (HRExecutive) is placed just before the name of the
    variable, interviewer, to cast it to HRExecutive. A pair of parentheses surrounds
    HRExecutive, which lets Java know you’re sure that the object being referred to is an
    object of the class HRExecutive. Casting is another method of telling Java, “Look, I
    know that the actual object being referred to is HRExecutive, even though I’m using a
    reference variable of type Interviewer.”
    */
  }
}
class Employee {}
interface Interviewer {
public void conductInterview();
}
class HRExecutive extends Employee implements Interviewer {
String[] specialization;
public void conductInterview() {
System.out.println("HRExecutive - conducting interview");
}
}
class Manager implements Interviewer{
int teamSize;
public void conductInterview() {
System.out.println("Manager - conducting interview");
}
}
