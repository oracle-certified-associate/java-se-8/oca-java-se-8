/*
■ B assigns the object referred to by myArrList to assignedArrList. The variables myArrList
and assignedArrList now refer to the same object.
■ C assigns a copy of the object referred to by myArrList to clonedArrList.
The variables myArrList and clonedArrList refer to different objects.
DETAIL: Because the method clone returns a value of the type Object, it’s cast to
ArrayList<StringBuilder> to assign it to clonedArrList.
■ D prints true because myArrList and assignedArrList refer to the same object.
■ DETAIL: E prints false because myArrList and clonedArrList refer to separate objects,
because the method clone creates and returns a new object of ArrayList (but with the same list members).
■ F proves that the method clone didn’t copy the elements of myArrList. All the
variable references myArrVal, AssignedArrVal, and clonedArrVal refer to the
same objects.
■ Hence, both G and H print true.
*/
class arraylistCloning{
  public static void main (String args[]){
    ArrayList<StringBuilder> myArrList = new ArrayList<StringBuilder>();
    StringBuilder sb1 = new StringBuilder("Jan");
    StringBuilder sb2 = new StringBuilder("Feb");
    myArrList.add(sb1);
    myArrList.add(sb2);
    myArrList.add(sb2);
    ArrayList<StringBuilder> assignedArrList = myArrList;//B
    ArrayList<StringBuilder> clonedArrList = (ArrayList<StringBuilder>)myArrList.clone();//C
    System.out.println(myArrList == assignedArrList);//D
    System.out.println(myArrList == clonedArrList);//E
    StringBuilder myArrVal = myArrList.get(0);
    StringBuilder assignedArrVal = assignedArrList.get(0);
    StringBuilder clonedArrVal = clonedArrList.get(0);
    System.out.println(myArrVal == assignedArrVal);//G
    System.out.println(myArrVal == clonedArrVal);//H
  }
}
/*
DETAIL:
You can use the method toArray to return an array containing all the elements in an ArrayList
in sequence from the first to the last element.
An ArrayList uses a private variable, elementData (an array), to store its own values.
Method toArray doesn’t return a reference to this array. It creates a new array, copies the elements of
the ArrayList to it, and then returns it.
Now comes the tricky part. No references to the returned array, which is itself an object, are maintained
by the ArrayList. But the references to the individual ArrayList elements are copied to the returned
array and are still referred to by the ArrayList.
This implies that if you modify the returned array by, say, swapping the position of its elements
or by assigning new objects to its elements, the elements of ArrayList won’t be affected. But if you modify
the state of (mutable) elements of the returned array, then the modified state of elements will be
reflected in the ArrayList.
*/
