/*
If the return type of a method is int, the method can return a value of type byte.
*/
public class returnByte{
  private byte myByte;
  public static void main(String... args){
    returnByte rb = new returnByte();
    rb.returnInt();
  }
  public returnByte(){
    //
  }
  private returnByte(int someInt){//you can use all 4 access modifiers while defining constructors
    myByte = (byte)someInt;//you can't assign bigger type to smaller unless you explicityly cast
  }
  public int returnInt(){
    byte myByte = 7;
    System.out.println(myByte);
    return myByte;
    //return type of the method is int and we can return a byte value here
    //which is an integer type primitive value
  }
}
