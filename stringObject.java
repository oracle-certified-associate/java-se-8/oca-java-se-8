/*
The terms String constant pool and String pool are used interchangeably
and refer to the same pool of String objects. Because String objects are
immutable, the pool of String objects is also called the String constant pool.
You may see either of these terms on the exam.
*/
public class stringObject{
  public static void main(String args[]){
    /*
    When the following line of code executes, no String object with the value
    "Harry" is found in the pool of String objects:
    */
    String str3 = "Harry";
    /*
    When the following line of code executes, Java is able to find a String object with the
    value "Harry" in the pool of String objects
    Java doesn’t create a new String object in this case, and the variable str4 refers to the existing String object "Harry".
    */
    String str4 = "Harry";
    System.out.println(str3 == str4);//true

    String morning1 = "Morning";
    System.out.println("Morning" == morning1);//true

    String morning2 = new String("Morning");
    System.out.println("Morning" == morning2);//false

    //You can also create a String object by enclosing a value within double quotes
    System.out.println("this generates a new string object");

    String girl = new String("Shreya");//String constructor that accepts a String
    char[] name = new char[]{'P','a','u','l'};
    String boy = new String(name);//String constructor that accepts a char array

    StringBuilder sd1 = new StringBuilder("String Builder");
    String str5 = new String(sd1);//String constructor that acceps objects of StringBuilder
    StringBuffer sb2 = new StringBuffer("String Buffer");
    String str6 = new String(sb2);//String constructor that acceps objects of StringBuffer
  }
}
