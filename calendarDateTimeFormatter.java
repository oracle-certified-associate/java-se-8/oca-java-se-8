/*
You can instantiate or access a DateTimeFormatter object in multiple ways:
■ By calling a static ofXXX method, passing it a FormatStyle value
■ By access public static fields of DateTimeFormatter
■ By using the static method ofPattern and passing it a string value
*/
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.Month;
class calendarDateTimeFormatter{
  public static void main(String... args){
    /*
    The methods ofLocalizedDate, ofLocalizedTime, and ofLocalizedDateTime format date
    and time objects according to the locale (language, region, or country) of the system
    on which your code executes. So the output might vary slightly across systems.
    */
    DateTimeFormatter formatter11 = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);
    DateTimeFormatter formatter12 = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG);
    DateTimeFormatter formatter13 = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM);
    DateTimeFormatter formatter14 = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);

    DateTimeFormatter formatter2 = DateTimeFormatter.ofLocalizedTime(FormatStyle.FULL);
    DateTimeFormatter formatter3 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG);
    DateTimeFormatter formatter4 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT, FormatStyle.SHORT);
    LocalDate date1 = LocalDate.of(2057,8,11);
    LocalTime time1 = LocalTime.of(15,14,14);
    System.out.println(time1);
    LocalDateTime dateTime1 = LocalDateTime.of(2050, 6, 5, 14, 30, 0);
/*
    LocalDate ld = LocalDate.of(2014, Month.JUNE, 21);
    LocalTime lt = LocalTime.of(17, 30, 20);
    LocalDateTime ldt = LocalDateTime.of(ld, lt);
*/
    System.out.println(formatter11.format(date1));//Saturday, August 11, 2057
    System.out.println(formatter12.format(date1));//August 11, 2057
    System.out.println(formatter13.format(date1));//Aug 11, 2057
    System.out.println(formatter14.format(date1));//8/11/57
    //System.out.println(formatter2.format(time1));
    //System.out.println(formatter3.format(dateTime1));
    //System.out.println(formatter3.format(ldt));
    System.out.println(formatter4.format(dateTime1));//6/5/50 2:30 PM

    DateTimeFormatter formatter51 = DateTimeFormatter.BASIC_ISO_DATE;
    DateTimeFormatter formatter52 = DateTimeFormatter.ISO_DATE;
    DateTimeFormatter formatter53 = DateTimeFormatter.ISO_LOCAL_DATE;
    DateTimeFormatter formatter54 = DateTimeFormatter.ISO_TIME;
    DateTimeFormatter formatter55 = DateTimeFormatter.ISO_LOCAL_TIME;
    DateTimeFormatter formatter56 = DateTimeFormatter.ISO_DATE_TIME;
    DateTimeFormatter formatter57 = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    System.out.println(formatter51.format(date1));//20570811
    System.out.println(formatter52.format(date1));//2057-08-11
    System.out.println(formatter53.format(date1));//2057-08-11
    System.out.println(formatter54.format(time1));//15:14:14
    System.out.println(formatter55.format(time1));//15:14:14
    System.out.println(formatter56.format(dateTime1));//2050-06-05T14:30:00
    System.out.println(formatter56.format(dateTime1));//2050-06-05T14:30:00

    System.out.println("xxxxxxxxxxxxxxxxxxxxx");

    /*
    Formatting date and time objects using DateTimeFormatter, which are created using string
    patterns, is interesting (and confusing). Take note of the case of the letters used in
    the patterns. M and m or D and d are not the same. Also, using a pattern letter doesn’t
    specify the count of digits or texts. For an example, using Y or YYYY to format a date object
    returns the same results.

    You can also format date and time objects by calling the format method in date or time objects
    and passing it a DateTimeFormatter instance.
    If you access Java’s source code, you’ll notice that the format and parse methods in date and
    time classes simply call the format and parse methods on a DateTimeFormatter instance.
    */

    LocalDate date = LocalDate.of(2057,8,11);
    LocalTime time = LocalTime.of(14,30,15);
    DateTimeFormatter d1 = DateTimeFormatter.ofPattern("y");
    DateTimeFormatter d2 = DateTimeFormatter.ofPattern("YYYY");
    DateTimeFormatter d3 = DateTimeFormatter.ofPattern("Y M D");
    DateTimeFormatter d4 = DateTimeFormatter.ofPattern("e");
    DateTimeFormatter t1 = DateTimeFormatter.ofPattern("H h m s");
    DateTimeFormatter t2 = DateTimeFormatter.ofPattern("'Time now:'HH mm a");
    System.out.println(d1.format(date));
    System.out.println(d2.format(date));
    System.out.println(d3.format(date));
    System.out.println(d4.format(date));
    System.out.println(t1.format(time));
    System.out.println(t2.format(time));

    /*
    To parse a date or time object, you can use either the "static" parse method in date/time
    objects or the "instance" parse method in the DateTimeFormatter class.
    Behind the scenes, the parse method in date/time objects simply calls the parse method in
    DateTimeFormatter
    When calling parse on LocalDate, LocalTime, or LocalDateTime
    instances, you might not specify a formatter. In this case DateTimeFormatter.
    ISO_LOCAL_DATE, DateTimeFormatter.ISO_LOCAL_TIME, and DateTimeFormatter.ISO_LOCAL_DATE_TIME are used to parse text, respectively.
    */
    DateTimeFormatter d5 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    LocalDate date5 = LocalDate.parse("2057-01-29", d5 );
    /*
    The following line throws a DateTimeParseException because this mechanism works
    only if all components are present
    */
    DateTimeFormatter d6 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    //LocalDate date6 = LocalDate.parse("2057", d6);//this will throw an exception. -> could not be parsed

    DateTimeFormatter d7 = DateTimeFormatter.ofPattern("yyyy");
    //LocalDate date7 = LocalDate.parse("2057-01-29", d7);//this will throw an exception. -> unparsed text found

    DateTimeFormatter d8 = DateTimeFormatter.ofPattern("yyyy");
    //LocalDate date8 = LocalDate.parse("2057", d8);//this will throw an exception. Unable to obtain LocalDate rom TemporalAccessor

    DateTimeFormatter d9 = DateTimeFormatter.ofPattern("dd-yyyy-MM");
    LocalDate date9 = LocalDate.parse("29-2057-01", d9);//this works
  }
}
