/*
A class can implement multiple interfaces that define static methods with the same
name, even if they don’t qualify as correctly overloaded or overridden methods. This
is because they’re not inherited by the class that implements the interfaces.
*/
interface Jumpable {
  static int maxDistance() {
    return 100;
  }
}
interface Moveable {
  static String maxDistance() {
    return "forest";
  }
}
class Animal implements Jumpable, Moveable { }
