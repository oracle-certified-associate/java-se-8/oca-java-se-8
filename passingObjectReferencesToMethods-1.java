/*
There are 2 cases:
■ WHEN METHODS REASSIGN THE OBJECT REFERENCES PASSED TO THEM <---
■ When a method modifies the state of the object reference passed to it

When a method is passed a reference value, a copy of the reference (that
is, the memory address) is passed to the invoked method. The callee can do whatever it
wants with its copy without ever altering the original reference held by the caller.

In the preceding code, B creates two object references, person1 and person2.
line 37 prints John:Paul—the value of person1.name and person2.name.
The code then calls the method swap and passes to it the objects referred to by
person1 and person2. When these objects are passed as arguments to the method
swap, the method arguments p1 and p2 also refer to these objects.
*/
class Person {
  private String name;
  Person(String newName) {
    name = newName;
  }
  public String getName() {
    return name;
  }
  public void setName(String val) {
    name = val;
  }
}
class Test {
  public static void swap(Person p1, Person p2) {
    Person temp = p1;
    p1 = p2;
    p2 = temp;
  }
  public static void main(String args[]) {
    Person person1 = new Person("John");
    Person person2 = new Person("Paul");
    System.out.println(person1.getName() + ":" + person2.getName());//line 37
    swap(person1, person2);
    System.out.println(person1.getName() + ":" + person2.getName());//line 39
  }
}
/*
The reference variables person1 and person2 are still
referring to the objects that they passed to the method swap. Because no change was
made to the values of the objects referred to by variables person1 and person2, line 39vprints John:Paul again.
*/
