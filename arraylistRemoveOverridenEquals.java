/*
The method "remove" compares the objects for equality
before removing it from ArrayList by calling the method equals.

DETAIL:
You can override the equals methods in your classes to change the way objects are
compared for equality and hence change the conditions of their removal from an Arraylist.!
*/
import java.util.ArrayList;
class MyPerson {
  String name;
  MyPerson(String name)
  {
    this.name = name;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof MyPerson) {
      MyPerson p = (MyPerson)obj;
      boolean isEqual = p.name.equals(this.name);
      return isEqual;
  }
  else
    return false;
  }
}

public class arraylistRemoveOverridenEquals {
  public static void main(String args[]) {
    ArrayList<MyPerson> myArrList = new ArrayList<MyPerson>();
    MyPerson p1 = new MyPerson("Shreya");
    MyPerson p2 = new MyPerson("Paul");
    MyPerson p3 = new MyPerson("Harry");
    myArrList.add(p1);
    myArrList.add(p2);
    myArrList.add(p3);
    /*
    Below line removes Paul object added before. Because in the overloaded equals method of MyPerson
    class we only check that object type and the value of name property for equality.
    */
    myArrList.remove(new MyPerson("Paul"));
    for (MyPerson element:myArrList)
      System.out.println(element.name);
  }
}
