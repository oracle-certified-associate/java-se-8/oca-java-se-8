/*
It’s acceptable for a class to extend multiple
interfaces that define abstract methods with the same signature because when a
class implements the abstract method, it seems to implement the abstract method
from all the interfaces.
*/
interface Jumpable {
  abstract String currentPosition();
}
interface Moveable {
  abstract String currentPosition();
}
class Animal implements Jumpable, Moveable {
  public String currentPosition() {
    return "Home";
  }
}

/*
But you can’t make a class extend multiple interfaces that define methods with the same
name that don’t seem to be a correct combination of overloaded methods. If you
change the return type of the method currentPosition() from String to void in the
interface Moveable, the class Animal won’t compile. It would need to implement methods
currentPosition, which differ only in their return type, which isn’t acceptable.
*/
interface Jumpable2 {
  abstract String currentPosition();
}
interface Moveable2 {
  abstract void currentPosition();
}
class Animal2 implements Jumpable2, Moveable2 { //-> this won't compile
  public String currentPosition() {
    return "Home";
  }
}
/*
TIP:
A class can implement multiple interfaces with the same abstract
method names if they have the same signature or form an overloaded set of
methods.
*/
