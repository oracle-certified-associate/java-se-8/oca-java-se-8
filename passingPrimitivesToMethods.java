/*
The value of a primitive data type is copied and passed to a method. Hence, the variable
whose value was copied doesn’t change.

When the class Office calls the method modifyVal, it passes a copy of the value
of the object field age to it. The method modifyVal never accesses the object field
age. Hence, after the execution of this method, the value of the method field age
prints as 0 again.

Within a method, a method parameter takes precedence over an object field.
When the method modifyVal2 refers to the variable age, it refers to the method
parameter age, not the instance variable age. To access the instance variable
age within the method modifyVal2, the variable name age needs to be prefixed
with the keyword this (this is a keyword that refers to the object itself).
*/
class Employee {
  int age;
  void modifyVal(int a) {
    a = a + 1;
    System.out.println(a);
  }
  void modifyVal2(int age){
    age += 1;
    System.out.println(age);
  }
}
class Office {
  public static void main(String args[]) {
    Employee e = new Employee();
    System.out.println(e.age);//prints 0
    e.modifyVal(e.age);//prints 1
    System.out.println(e.age);//prints 0
    e.modifyVal2(e.age);//prints 1
    System.out.println(e.age);//prints 0
  }
}
