/*
Below, the operator on top has the highest precedence, and
operators within the same group have the same precedence and are evaluated from left to right.

Postfix >> Expression++, expression--
Unary >> ++expression, --expression, +expression, -expression, !
Multiplication >> * (multiply), / (divide), % (remainder)
Addition >> + (add), - (subtract)
Relational >> <, >, <=, >=
Equality >> ==, !=
Logical AND >> &&
Logical OR >> ||
Assignment >> =, +=, -=, *=, /=, %=

You can use parentheses to override the default operator precedence.
If your expression defines multiple operators and you’re unsure how your
expression will be evaluated, use parentheses to evaluate in your preferred
order. The inner parentheses are evaluated prior to the outer ones, following
the same rules of classic algebra.
*/
public class operatorPresedence{
  public static void main(String... args){
    int int1 = 10, int2 = 20, int3 = 30;
    System.out.println(int1 % int2 * int3 + int1 / int2); //prints 300. remember int1/int2 equals 0 as an integer
    System.out.println(int1 % int2 * (int3 + int1) / int2); //Use parentheses to override the default operator precedence. Prints 20.
  }
}
