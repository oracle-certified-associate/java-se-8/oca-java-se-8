/*

*/
class Employee {
    static void defaultPlan() {
        System.out.println("Basic");
    }
}
class Programmer extends Employee {}

class Project {
  public static void main(String[] args) {
    Employee emp = new Programmer();
    emp.defaultPlan();//static method accessed using variable emp

    Programmer pgr = new Programmer();
    pgr.defaultPlan();//static method accessed using variable pgr

    Employee.defaultPlan();//static method accessed using Employee
    Programmer.defaultPlan();//static method accessed using Programmer
  }
}
