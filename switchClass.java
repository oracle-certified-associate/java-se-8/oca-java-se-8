/*
DETAIL
The value of a "case label" must be a compile-time constant value; that is, the value
should be known at the time of code compilation.
DETAIL
You may be surprised to learn that if you don’t assign a value to a final variable
with its declaration, it isn’t considered a compile-time constant.
*/

class switchClass{
  public static void main(String args[]){
    int a=10, b=20, c=30;
    final int af = 11;
    final int bf = 21;
    final int cf;
    cf = 31;
    switch (a) {
      // case b+c: //this is not allowed. should be compile time constant.
      // System.out.println(b+c);
      // break;
      case 10*7:
      System.out.println(10*7512+10);
      break;
      /*
      You can use variables in an expression if they’re marked final because the value
      of final variables can’t change once they’re initialized
      */
      case af+bf://this is allowed
      break;
      /*
      case cf://this is not allowed. should be compile time constant.
      break;
      */
    }
    //CASE VALUES SHOULD BE ASSIGNABLE TO THE ARGUMENT PASSED TO THE SWITCH STATEMENT
    byte myByte = 10;
      switch (myByte) {
        /*
        case 1.2://This won't compile
        System.out.println(1);
        break;
        */
        /*
        case null:  //NULL ISN’T ALLOWED AS A CASE LABEL
        System.out.println("null");
        break;
        */
    }
    //ONE CODE BLOCK CAN BE DEFINED FOR MULTIPLE CASES
    int score =10;
    switch (score) {
      case 70:
      case 50 :
      case 10 :
      System.out.println("Average score");
      break;
      case 100: System.out.println("Good score");
      break;
    }
  }
}
