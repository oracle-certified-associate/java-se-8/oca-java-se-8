/*
The variables that accept values in a method signature are called method parameters.
They’re accessible only in the method that defines them.
*/
class Phone {
  private boolean tested;
  public void setTested(boolean val) { //Method parameter val is accessible only in method setTested
    tested = val;
  }
  public boolean isTested() {
    val = false; //val can't be accessed in this method. This line won't compile.
    return tested;
  }
  /*
  The scope of a method parameter may be as long as that of a local variable or longer,
  but it can never be shorter.
  */
  boolean isPrime(int num) {//The scope of the method parameter num is as long as the scope of the local variable result
    if (num <= 1) return false;
    boolean result = true;
    for (int ctr = num-1; ctr > 1; ctr--) { //local variable ctr is limited to the for block
      if (num%ctr == 0) result = false;
    }
    return result;
  }
}
