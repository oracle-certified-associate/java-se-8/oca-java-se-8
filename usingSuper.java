/*
"super" refers to the direct parent or base class of a class.
"super" can be used to access a variable or method from the base class if there’s a clash between these names (same names used
both in derived class and in base class).

The keywords super and this are implicit object references. Because static methods
belong to a class, not to objects of a class, you can’t use this and super in static
methods.
*/
class usingSuper{
  public static void main(String args[]){
    Programmer programmer = new Programmer();
    programmer.setNames();
    programmer.printNames();
  }
}
class Employee {
  String name;
}
class Programmer extends Employee {
  String name;
  void setNames() {
    name = "Programmer";//same as below. sets value for this.name
    this.name = "Programmer";
    super.name = "Employee";
  }
  void printNames() {
    System.out.println(super.name);
    System.out.println(this.name);
    System.out.println(name);// pprints value of this.name
  }
}
class Employee2 {
  String name;
  String address;
  Employee2(String name, String address) {
    this.name = name;
    this.address = address;
  }
}
/*
If present, a call to a superclass’s constructor must be the first
statement in a derived class’s constructor. Otherwise, a call to super(); (the
no-argument constructor) is inserted automatically by the compiler.
*/
class Programmer2 extends Employee2 {
  String progLanguage;
  Programmer2(String name, String address, String progLang) {
    super(name, address);
    this.progLanguage = progLang;
  }
}
