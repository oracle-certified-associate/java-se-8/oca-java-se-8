/*
An array is an object itself; it stores references to the data it stores. Arrays can store two types of data:
■ A collection of primitive data types
■ A collection of objects
An array of primitives stores a collection of values that constitute the primitive values themselves.
An array of objects stores a collection of values, which are in fact heap-memory addresses or pointers.
The members of an array are defined in contiguous (continuous) memory locations and hence offer improved access speed.

In Java, you can define one-dimensional and multidimensional arrays.
Note that multidimensional arrays may or may not contain the same number of elements in each row or column.

An array type can be any of the following:
■ Primitive data type
■ Interface
■ Abstract class
■ Concrete class
*/
class arrays{
  public static void main(String args[]){
    //below 3 lines are the same
    int[] myArray1[];
    int[][] myArray2;
    int myArray3[][];

    //below 2 lines are the same
    int[] myArray4;
    int myArray5[];
    /*
    - The array declaration only creates a variable that refers to null.
    - In an array declaration, placing the square brackets next to the type
    (as in int[] or int[][]) is preferred because it makes the code easier to read by showing the array types in use.
    - Because no elements of an array are created when it’s declared, it’s invalid to define
    the size of an array with its declaration. Below 3 lines won't compile:
    int intArray[2];
    String[5] strArray;
    int[2] multiArray[3];

    - Because an array is an object, it’s allocated using the keyword new, followed by the type
    of value that it stores, and then its size. The code won’t compile if you don’t specify
    the size of the array or if you place the array size on the left of the = sign.
    Below 2 lines won't compile:
    intArray = new int[];
    intArray[2] = new int;
    */
    int[] intArray;
    int[][] multiArr,multiArr2;
    String[] strArray;
    intArray = new int[2];
    strArray = new String[4];
    multiArr = new int[2][3];//Once allocated array elements store their default values

    /*
    When you initialize a two-dimensional array, you can use nested for loops to initialize
    its array elements. Also notice that to access an element in a two-dimensional array,
    you should use two array position values, as follows
    */
    for (int i=0; i<multiArr.length; i++) {
      for (int j=0; j<multiArr[i].length; j++) {
        multiArr[i][j] = i + j;
      }
    }
    /*
    Once allocated, all the array elements store their default values.
    Elements in an array that store objects default to null. Elements of an array
    that store primitive data types store 0 for integer types (byte, short, int,
    long); 0.0 for decimal types (float and double); false for boolean; or
    \u0000 for char data.
    */

    multiArr2 = new int[2][];//OK to define the size in only the first square bracket
    /*
    you can’t allocate a multidimensional array without including a size in the first square
    bracket and defining a size in the second square bracket:
    multiArr = new int[][3]; -> this won't compile
    */

    //Java accepts an expression to specify the size of an array, as long as it evaluates to an int value.
    String[] strArray2, strArray3, strArray4;
    strArray2 = new String[2*5];
    int x = 10, y = 4;
    strArray3 = new String[x*y];
    strArray4 = new String[Math.max(2, 3)];

    /*
    DETAIL:
    Code to access an array index will throw a "runtime exception" if
    you pass it an invalid array index value. Code to access an array index will "fail
    to compile" if you don’t use a char, byte, short, or int.

    System.out.println(intArray[1.2]); -> won't compile
    System.out.println(intArray[-10]); -> will compile but result in runtime exception
    */

    String[] strArray5 = new String[] {"Autumn", "Summer", "Spring", "Winter"};//creates an array of String and initializes it with four String values
    strArray5[2] = null;
    for (String val : strArray5)
      System.out.println(val);
  }
}
