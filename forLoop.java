/*
It’s a common programming mistake to try to use the variables defined in a for’s initialization
block outside the for block. Please note that the scope of the variables
declared in the initialization block is limited to the for block.

A for loop can define exactly one termination condition—no more, no less.

All three parts of a for statement—initialization block, termination
condition, and update clause—are optional. A missing termination condition
implies an infinite loop.
*/
public class forLoop{
  public static void main(String... args){
    int tableOf = 25;
    /*
    A for loop can declare and initialize multiple
    variables in its initialization block, but the variables it declares should be of the
    same type.
    */
    for (int ctr = 1, num = 100000; ctr <= 5; ++ctr) {
      System.out.println(tableOf * ctr);
      System.out.println(num * ctr);
    }
    /*
    But you can’t declare variables of different types in an initialization block. The following
    code will fail to compile
    */
    /*
    for (int j=10, long longVar = 10; j <= 15; ++j) {//fails to compile
      System.out.println("fails to compile.");
    }
    */
    //DETAIL
    //You can define multiple statements in the update clause, including calls to other methods.
    String line = "ab";
    for (int i=0; i < line.length(); ++i, printMethod()){
      System.out.println(line.charAt(i));
    }
    /*
    All three parts of a for statement—that is, initialization block, termination condition,
    and update clause—are optional.
    */
    int b = 10;
    for(; b < 5; ++b) {//DETAIL: Removing the semicolon that marks the end of the initialization block prevents the code from compiling. for(a < 5; ++a) -> won't compile
      System.out.println(b);
    }
    /*
    for(int a = 10; ; ++a) {//Missing termination condition implies infinite loop!
      //if you remove the semicolon that marks the end of the termination condition,
      //the code won’t compile. for(int a = 10; ++a) -> won't compile
      System.out.println(a);
    }
    */
    //The following code doesn’t include code in its update clause but compiles successfully
    //But removing the semicolon that marks the start of the update clause prevents the code from compiling. for(int a = 10; a > 5) -> won't compile.
    for(int a = 10; a > 5; ) {
      //System.out.println(a); //infinite loop
    }
    for(;;)//This is valid. Does nothing.
      System.out.println("for loop with none of the parts defined");

    /*
    Nested loops are often used to initialize or iterate multidimensional arrays. The following
    code initializes a multidimensional array using nested for loops
    */

    int multiArr[][];
    multiArr = new int[2][3];
    for (int i = 0; i < multiArr.length; i++) {//multiArr.length  = 2
      for (int j = 0; j < multiArr[i].length; j++) {
        multiArr[i][j] = i + j;
      }
    }

  }
  private static void printMethod() {
    System.out.println("Happy");
  }
}
