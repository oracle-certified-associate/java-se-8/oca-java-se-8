/*
Here assigning 122 to myChar would result in displaying the character 'z'
but forcing a cast from a negative int to a char variable will result in
strange results.

The char data type in Java doesn’t allocate space to store the sign of an integer. If you
try to forcefully assign a negative integer to char, the sign bit is stored as the part of
the integer value, which results in the storage of unexpected values.
*/
public class castNegativeIntToChar{
  public static void main(String... args){
    char myChar = (char)-122;
    System.out.println(myChar);
  }
}
