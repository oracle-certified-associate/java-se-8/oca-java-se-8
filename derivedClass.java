/*
A derived class can inherit only what it can see. A derived class inherits all the
nonprivate members of its base class. A derived class inherits base class members
with the following accessibility levels:
■ default: Members with default access can be accessed in a derived class only if
the base and derived classes reside in the same package.
■ protected: Members with protected access are accessible to all the derived
classes, regardless of the packages in which the base and derived classes are
defined.
■ public: Members with public access are visible to all other classes.
*/
