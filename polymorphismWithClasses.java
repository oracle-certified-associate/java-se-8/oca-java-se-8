/*
Polymorphism with classes comes into the picture when a class inherits another class
and both the base and the derived classes define methods with the same method signature.

An object can also be referred to using a reference variable of its base
class. In this case, depending on the type of the object used to execute a method, the
Java runtime executes the method defined in the base or derived class.

The objects are aware of their own type and execute the overridden method defined in
their own class, even if a base class variable is used to refer to them.

OVERRIDING:
Note that the name of the method startProjectWork is same in all these classes. Also,
it accepts the same number of method arguments and defines the same return type in
all three classes: Employee, Programmer, and Manager. This is a contract specified to
define overridden methods. Failing to use the same method name, same argument
list, or same return type won’t mark a method as an overridden method.

An overridden method defined in the base class can be an abstract method or
a non-abstract method. You get the same results.

DETAIL:
The return type of an overriding method in the subclass can be the same as or a
subclass of the return type of the overridden method in the base class (covariant return type).

Access modifiers for an overriding method can be the same as or less restrictive
than the method being overridden, but they can’t be more restrictive.

OVERRIDE vs OVERLOAD

Polymorphism works only with overridden methods! Overridden methods have the
same number and type of method arguments, whereas overloaded methods define a
method argument list with either a different number or type of method parameters.
*/
class polymorphismWithClasses{
  public static void main(String... args){
    Employee emp1 = new Programmer();
    Employee emp2 = new Manager();
    emp1.reachOffice();
    emp2.reachOffice();
    emp1.startProjectWork();
    emp2.startProjectWork();
  }
}
abstract class Employee {
  public void reachOffice() {
    System.out.println("reached office - Gurgaon, India");
  }
  public abstract void startProjectWork();
}
class Programmer extends Employee {
  public void startProjectWork() {
    defineClasses();
    unitTestCode();
  }
  private void defineClasses() { System.out.println("define classes"); }
  private void unitTestCode() { System.out.println("unit test code"); }
}
class Manager extends Employee {
  public void startProjectWork() {
    meetingWithCustomer();
    defineProjectSchedule();
    assignRespToTeam();
  }
  private void meetingWithCustomer() {
    System.out.println("meet Customer");
  }
  private void defineProjectSchedule() {
    System.out.println("Project Schedule");
  }
  private void assignRespToTeam() {
    System.out.println("team work starts");
  }
}
