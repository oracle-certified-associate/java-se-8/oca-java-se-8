/*
Any Java class can define a set of rules to determine whether its two objects should be considered equal.
This comparison is accomplished using the method equals.
The method equals is defined in class java.lang.Object. All the Java classes directly or indirectly inherit this class.
As you can see, the default implementation of the equals method only compares whether two object variables
refer to the same object. Because instance variables are used to store the state of an object,
it’s common to compare the values of the instance variables to determine whether two objects should be considered equal.
*/
import java.util.ArrayList;
class BankAccount {
    String acctNumber;
    int acctType;
    public boolean equals(Object anObject) {
        if (anObject instanceof BankAccount) {
            BankAccount b = (BankAccount)anObject;
            return (acctNumber.equals(b.acctNumber) && acctType == b.acctType);
        }
        else
          return false;
    }
}
/*
DETAIL:
It’s a common mistake to write an equals method that accepts an instance of the class itself.
In the following code, the class BankAccount doesn’t override equals(); it overloads it.
*/
class BankAccount2 {
    String acctNumber;
    int acctType;
    public boolean equals(BankAccount2 anObject) {
        if (anObject instanceof BankAccount2) {
            BankAccount2 b = (BankAccount2)anObject;
            return (acctNumber.equals(b.acctNumber) && acctType == b.acctType);
        }
        else
          return false;
    }
}

class comparingObjects {
  public static void main(String args[]) {
    BankAccount b1 = new BankAccount();
    b1.acctNumber = "0023490";
    b1.acctType = 4;
    BankAccount b2 = new BankAccount();
    b2.acctNumber = "11223344";
    b2.acctType = 3;
    BankAccount b3 = new BankAccount();
    b3.acctNumber = "11223344";
    b3.acctType = 3;

    System.out.println(b1.equals(b2));//false
    System.out.println(b2.equals(b3));//true
    System.out.println(b1.equals(new String("abc")));//false

    BankAccount2 b21 = new BankAccount2();
    b21.acctNumber = "11223344";
    b21.acctType = 3;

    BankAccount2 b22 = new BankAccount2();
    b22.acctNumber = "11223344";
    b22.acctType = 3;

    ArrayList <BankAccount2> list = new ArrayList<BankAccount2>();
    list.add(b21);
    System.out.println(list.contains(b22));//false
    /*
    DETAIL:
    Because the class BankAccount2 didn’t follow the rules for correctly defining (overriding) the method equals,
    ArrayList uses the method equals from the base class Object, which compares object references.
    Because the code didn’t add b22 to list, it prints false.

    The method equals defines a method parameter of type Object, and its return type is boolean.
    Don’t change the name of the method, its return type, or the type of method parameter when you
    define (override) this method in your class to compare two objects.
    */
  }
}
