/*
The essence of an abstract class is it groups the
common properties and behavior of its derived classes, but it prevents itself from
being instantiated. Also, an abstract class can force all its derived classes to define
their own implementations for a behavior by defining it as an abstract method (a
method without a body).

It isn’t mandatory for an abstract class to define an abstract method. But if an
abstract base class defines one or more abstract methods, the class must be marked
as abstract and the abstract methods must be implemented in all its concrete
derived classes. If a derived class doesn’t implement all the abstract methods defined
by its base class, then it also needs to be an abstract class.

You can use variables of an abstract base class to refer to objects of its derived
class.
*/
