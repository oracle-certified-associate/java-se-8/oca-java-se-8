public class wrapperClassesAreImmutable{
  public static void main(String... args){
    ArrayList<Double> list = new ArrayList<Double>();
    list.add(12.12);
    list.add(11.24);
    Double total = 0.0;
    for (Double d : list)
      total += d;
  }
  /*
  DETAILS:
  wrapper classes are immutable. So what happens when you add a value to the variable total, a Double object?
  In this case, the variable total refers to a "new" Double object.
  */
}
