/*
■ Overloaded constructors must be defined using different argument lists.
■ Overloaded constructors can’t be defined by just a change in the access levels.
■ Overloaded constructors may be defined using different access levels.
■ A constructor can call another overloaded constructor by using the keyword this.
■ A constructor can’t invoke a constructor by using its class’s name.
■ If present, the call to another constructor must be the first statement in a
constructor.
■ You can’t call multiple constructors from a constructor.
■ A constructor can’t be invoked from a method (except by instantiating a class
using the new keyword).
■ A default constructor can’t coexist with other constructors.
A default constructor is automatically created by the Java compiler if the user doesn’t
define any constructor in a class. If the user reopens the source code file and adds a
constructor to the class, upon recompilation no default constructor will be created for the class.
*/
class Employee {
  String name;
  int age;
  Employee() {
    System.out.println("No-argument constructor");//won't compile. comment out this line. see below.
    this(null, 0);//Won’t compile—the call to the overloaded constructor must be the first statement in a constructor
    //So, by definition, you can’t call two (or more) constructors within a constructor because the call to a constructor must be the first statement in a constructor
    Employee(null, 0);//won't compile.  comment out this line. It’s a common mistake to try to invoke a constructor from another constructor using the class’s name.
  }
  Employee(String newName, int newAge) {
    name = newName;
    age = newAge;
  }
}
